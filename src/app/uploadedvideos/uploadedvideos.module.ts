import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UploadedvideosPageRoutingModule } from './uploadedvideos-routing.module';

import { UploadedvideosPage } from './uploadedvideos.page';
import { HeaderPageModule } from '../header/header.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UploadedvideosPageRoutingModule, HeaderPageModule
  ],
  declarations: [UploadedvideosPage]
})
export class UploadedvideosPageModule { }
