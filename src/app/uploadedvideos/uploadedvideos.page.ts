import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalServicesService } from '../global-services.service';
import { MenuController, AlertController, NavController, NavParams, PopoverController } from '@ionic/angular';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard';
import { IonInfiniteScroll, IonContent } from '@ionic/angular';

@Component({
  selector: 'app-uploadedvideos',
  templateUrl: './uploadedvideos.page.html',
  styleUrls: ['./uploadedvideos.page.scss'],
})
export class UploadedvideosPage implements OnInit {

  constructor(private streamingMedia: StreamingMedia, public service: GlobalServicesService, private menu: MenuController,
    public navCtrl: NavController, private router: Router) {

  }

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent) Content: IonContent;

  // =====================Hide Toolbar when keypad is open=====================
  showToolbar: boolean = true;
  UploadedVideosLists: any = [];
  totalRecordCount: any;
  selectedItem: any;
  page = 0;
  search = '';
  loadMoreBtn = false;
  ngOnInit() {
    this.onUploadedVideos();
    Keyboard.onKeyboardShow().subscribe(() => {
      this.showToolbar = false;
    });
    Keyboard.onKeyboardHide().subscribe(() => {
      this.showToolbar = true;
    });
    this.loadMore();
  }

  ionViewWillEnter() {
    this.service.sendMessage('startNotificationCountFunction');
  }

  ////=====================Video Player=====================
  videoUrl = this.service.STREAMING_VIDEO_URL;
  public play() {
    let options: StreamingVideoOptions = {
      successCallback: () => { },
      errorCallback: (e) => { console.log('Error streaming') },
      orientation: 'portrait',
      shouldAutoClose: true,
      controls: true
    };
    this.streamingMedia.playVideo(this.videoUrl, options);
  }

  // loadData(event) {
  //   setTimeout(() => {
  //     this.itemsPerPage = this.itemsPerPage + 12;
  //     this.onUploadedVideos();
  //     event.target.complete();
  //     if (this.UploadedVideosLists.length == this.totalRecordCount) {
  //       event.target.disabled = true;
  //     }
  //   }, 700);
  // }
  loadMore() {
    this.page = this.page + 1;
    this.loadMoreBtn = false;
    this.onUploadedVideos();
  }
  // ===================== UPLOADED VIDEO LIST =====================
  onUploadedVideos = () => {
    let username = localStorage.getItem('username');
    let obj = {
      'pageid': this.page,
      'rows_in_page': 12,
      'username': username,
      'image_video': 'VIDEO',
      'image_category': this.search
    };
    // if (this.isEnableSearch == true) {
    //   obj.image_category = this.selectedItem;
    // }
    this.service.post_data("getTrendSpotterImagesVideosWithPaging", obj)
      .subscribe((result) => {
        // let json: any = this.service.parserToJSON(result);
        let json: any = this.service.parserToJSON(result);
        let jons1 = JSON.stringify(json);
        let json2 = jons1.replace(/\\n/g, '')
        var parsedJSON = JSON.parse(JSON.parse(json2).string);


        this.totalRecordCount = parsedJSON.Total_Records;
        if (this.totalRecordCount >= this.UploadedVideosLists.length) {
          this.UploadedVideosLists = this.UploadedVideosLists.concat(parsedJSON.Details);
          setTimeout(() => {
            if (this.totalRecordCount! > this.UploadedVideosLists.length) {
              this.loadMoreBtn = true;
            } else {
              this.loadMoreBtn = false;
            }
          }, 500);
        }
      }, (error) => {
        console.log(error);
      });
  }

  //=====================Move to Inner Page of Selected Image=====================
  navigateMediaInfo() {
    this.router.navigate(['/media-info', { 'page': 'video' }]);
  }

  //=====================Open Menu=====================
  openMenu() {
    // this.menu.enable(true, 'first');
    this.menu.open();
  }

  gotoInnerPage(data) {
    this.router.navigate(['/media-info', { 'data': JSON.stringify(data), 'page': 'video' }]);

  }

  //=====================Search With Auto-Complete=====================
  // Declare the variable (in this case and initialize it with false)
  isItemAvailable = false;
  items = [];

  initializeItems() {
    this.items = ["Campus ch1 | Chennai", "Campus ch2 | Chennai", "Campus bg1| Bangalore"];
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
      this.isItemAvailable = true;
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    } else {
      this.isItemAvailable = false;
    }
  }

  searchList() {
    this.page = 1;
    this.UploadedVideosLists = [];
    this.onUploadedVideos();
  }
  // Hide/Show serach bar
  isEnableSearch: boolean;
  enableSearch(value) {
    this.isEnableSearch = value;
    if (!value && this.search) {
      // this.infiniteScroll.disabled = false;
      this.search = '';
      this.page = 1;
      this.UploadedVideosLists = [];
      this.onUploadedVideos();
    }
    this.Content.scrollToTop();
  }

  ItemAvailableClose(selected) {
    this.isItemAvailable = false;
    this.selectedItem = selected;
  }

}
