import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UploadedvideosPage } from './uploadedvideos.page';

describe('UploadedvideosPage', () => {
  let component: UploadedvideosPage;
  let fixture: ComponentFixture<UploadedvideosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadedvideosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadedvideosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
