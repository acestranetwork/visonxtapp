import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { IonicStorageModule } from '@ionic/storage';
import { HomePageRoutingModule } from './home-routing.module';
import { HeaderPageModule } from '../header/header.module';
import { AdvertisementModalpopPageModule } from '../advertisement-modalpop/advertisement-modalpop.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    IonicStorageModule,
    HeaderPageModule, AdvertisementModalpopPageModule
  ],
  declarations: [HomePage]
})
export class HomePageModule { }
