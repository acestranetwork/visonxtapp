import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { GlobalServicesService } from '../global-services.service';
import { ActionSheetController, MenuController, AlertController, NavController, NavParams, ViewWillEnter, ViewDidEnter, ModalController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import { Storage, IonicStorageModule } from '@ionic/storage';
import { Router, ActivatedRoute, ParamMap, NavigationExtras } from '@angular/router';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AdvertisementModalpopPage } from '../advertisement-modalpop/advertisement-modalpop.page';
import { VideoEditor, CreateThumbnailOptions } from '@ionic-native/video-editor';
// declare var CordovaExif: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [DatePipe]
})
export class HomePage implements OnInit, ViewWillEnter, ViewDidEnter {
  originalImage = null;
  fileInfo: any = {};
  notificationUrl = "getNotificationImage";
  options: GeolocationOptions;
  currentPos: Geoposition;
  locationTraces = [];
  latitude: any = 0; //latitude
  longitude: any = 0; //longitude
  address: string;
  imageMaxWidth: number = 720;
  imageMaxheight: number = 1024;
  imagePreview; //for dummy(getting image width and height)
  cameraOptions: CameraOptions = {
    destinationType: this.camera.DestinationType.FILE_URI, //this.camera.DestinationType.DATA_URL
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    // allowEdit: true,
    correctOrientation: true,
    sourceType: this.camera.PictureSourceType.CAMERA,
    quality: 50,
    // targetWidth: 500,
    // targetHeight: 720,
  }
  commentsInput: string = "";
  newCategory: string;
  posterImage = this.service.checkDomSanitizerForIOS('../../assets/image/play_overlay2.png');
  constructor(private storage: Storage,
    private mediaCapture: MediaCapture,
    private file: File,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public service: GlobalServicesService,
    private menu: MenuController,
    public navCtrl: NavController,
    private camera: Camera,
    private geolocation: Geolocation,
    private route: ActivatedRoute,
    private router: Router,
    private nativeGeocoder: NativeGeocoder,
    private webview: WebView,
    private modalController: ModalController,
    private datepipe: DatePipe) {
    this.userName = localStorage.getItem('username');
  }
  ngOnInit() {

    this.getnotificationDetails();
  }
  ionViewDidEnter() {

  }
  ionViewWillEnter() {
    this.service.sendMessage('startNotificationCountFunction');
    this.getRecentlyPostedList();
  }
  notificationCountFunction;
  userName;
  // =====================Actionsheet for mode of camera=====================
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Camera Mode',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Camera',
        role: 'camera',
        icon: 'camera-outline',
        handler: () => {
          this.takeSnap();
          // this.router.navigate(['/category-selection', { 'location': 'location', 'tempPath': '' , 'type': 'image', 'page':'home' }]);
          //  this.router.navigate(['/preview-filter', { 'location': 'location', 'tempPath': '' , 'type': 'image' }]);
        }
      }, {
        text: 'Video',
        icon: 'videocam-outline',
        handler: () => {
          this.captureVideo();
        }
      }]
    });
    await actionSheet.present();
  }

  //=====================Video Capture=====================
  captureVideo() {
    let options: CaptureVideoOptions = {
      limit: 1,
      duration: 30,
      quality: 50
    }

    this.mediaCapture.captureVideo(options).then((res: MediaFile[]) => {
      let capturedFile = res[0];
      this.fileInfo = {};
      capturedFile.getFormatData((data) => {
        this.fileInfo.resolution = data.width + 'x' + data.height + 'px';
        this.fileInfo.duration = data.duration;
        this.fileInfo.bitrate = data.bitrate;
        this.fileInfo.codecs = data.codecs;
      }, (error) => {
        console.log(error, 'getFormatData')
      })
      this.fileInfo.fileName = capturedFile.name;
      this.fileInfo.time = this.datepipe.transform(new Date(capturedFile.lastModifiedDate), 'yyyy/MM/dd hh:mm:ss a');
      this.fileInfo.size = Math.round((capturedFile.size / 1024 / 1024) * 100) / 100 + 'MB';
      this.fileInfo.type = capturedFile.type;
      if (capturedFile.fullPath.indexOf("file://") !== -1) {
        this.moveToPreview(this.address, capturedFile.fullPath, 'video');
      } else {
        this.moveToPreview(this.address, "file://" + capturedFile.fullPath, 'video');
      }
    },
      (err: CaptureError) => console.error(err));
  }

  //=====================Capture Image=====================
  takeSnap() {
    this.camera.getPicture(this.cameraOptions).then((imageData) => {
      this.fileInfo = {};
      this.file.resolveLocalFilesystemUrl(imageData).then(fileEntry => {
        fileEntry.getMetadata((metadata) => {
          let fileSize = metadata.size / 1024 / 1024;
          this.fileInfo.fileName = fileEntry.name;
          this.fileInfo.time = this.datepipe.transform(new Date(metadata.modificationTime), 'yyyy/MM/dd hh:mm:ss a');
          this.fileInfo.size = Math.round(fileSize * 100) / 100 + 'MB';
        });
      });
      //   if(CordovaExif){
      //   CordovaExif.readData(imageData, (res) => {
      //     this.fileInfo.resolution = res.ImageWidth + 'x' + res.ImageHeight + 'px';
      //     this.fileInfo.device = res.Make;
      //     this.fileInfo.model = res.Model;
      //     this.fileInfo.flash = res.Flash == 'Flash fired, compulsory flash mode' ? 'Flash Fired' : 'No Flash';
      //     this.fileInfo.iso = res.ISOSpeedRatings;
      //     this.fileInfo.whiteBalance = res.WhiteBalance;
      //     this.fileInfo.orientation = res.Orientation == 1 ? 'Portrait' : 'Landscape';
      //     this.fileInfo.latitude = res.GPSLatitude ? res.GPSLatitude : null;
      //     this.fileInfo.longitude = res.GPSLongitude ? res.GPSLongitude : null;
      //     // this.fileInfo.time = res.DateTime;
      //   })
      // }
      if (imageData.indexOf("file://") !== -1) {
        this.imagePreview = this.webview.convertFileSrc(imageData);
        this.moveToPreview(this.address, imageData, 'image');
      } else {
        this.imagePreview = this.webview.convertFileSrc("file://" + imageData);
        this.moveToPreview(this.address, "file://" + imageData, 'image');
      }

    }, (error) => {
      console.log(error);
    });
  }


  //=====================Move to Preview And Filter Page=====================
  moveToPreview(location, tempPath, type) {
    setTimeout(() => {
      if (tempPath) {

        if (localStorage.getItem('originalImg') != undefined) {
          localStorage.removeItem('originalImg');
        }
        if (localStorage.getItem('editedFilters') != undefined) {
          localStorage.removeItem('editedFilters');
        }
        if (localStorage.getItem('videoFileSize') != undefined) {
          localStorage.removeItem('videoFileSize')
        }
        if (localStorage.removeItem('fileSize') != undefined) {
          localStorage.removeItem('fileSize');
        }

        if (type == 'video') {
          this.router.navigate(['/preview-filter', { 'fileinfo': JSON.stringify(this.fileInfo), 'location': location, 'latitude': this.latitude, 'longitude': this.longitude, 'tempPath': tempPath, 'type': type, 'page': 'home' }]);
        } else {
          this.service.changeMessage(tempPath);
          this.router.navigate(['/preview-filter', { 'fileinfo': JSON.stringify(this.fileInfo), 'location': location, 'latitude': this.latitude, 'longitude': this.longitude, 'tempPath': tempPath, 'type': type, 'page': 'home' }]);
        }
      } else {
        this.presentAlert();
      }
    }, 1000)
  }

  async presentAlert3() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      subHeader: 'Image size Invalid',
      message: 'file too big, please select less than 8 MB',
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentAlert4() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      subHeader: 'Video size Invalid',
      message: 'file too big, please select less than 15 MB',
      buttons: ['OK']
    });

    await alert.present();
  }
  //=====================Open Menu=====================
  openMenu() {
    this.menu.open();
  }

  //=====================Validate File Empty=====================
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      message: 'Please Select the File', backdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          cssClass: 'warning',
        }
      ]
    });
    await alert.present();
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      subHeader: 'Image resolution size Invalid',
      message: 'please select the image resolution 200DPI size of 1280px X 720px',
      buttons: ['OK']
    });

    await alert.present();
  }
  //=====================get Login Details=====================
  loggedDetails;
  userInfo() {
    if (localStorage.getItem('username')) {
      let obj = {
        username: localStorage.getItem('username'),
      }
      this.service.post_data("getAlumniOthersInfo", obj)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          var parsedJSON = JSON.parse(json.string);
          if (parsedJSON[0].Status == "SUCCESS") {
            this.loggedDetails = parsedJSON[0];
          }
        }, (error) => {
          console.log(error);
        });

    }
  }

  //=====================get recently posted images Details=====================
  postedList;
  getRecentlyPostedList() {
    if (localStorage.getItem('username')) {
      let obj = {
        username: localStorage.getItem('username'),
        pageid: 1,
        rows_in_page: 6,
        image_video: 'ALL',
        image_category: 'ALL'
      }
      this.service.post_data("getTrendSpotterImagesVideosRecentWithPaging", obj)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          let jons1 = JSON.stringify(json);
          let json2 = jons1.replace(/\\n/g, '')
          var parsedJSON = JSON.parse(JSON.parse(json2).string);


          if (parsedJSON.Total_Records > 0) {
            this.postedList = parsedJSON.Details;
          }
        }, (error) => {
          console.log(error);
        });
    }
  }

  gotoInnerPage(data) {
    this.router.navigate(['/media-info', { 'data': JSON.stringify(data), 'page': 'home' }]);
  }


  getnotificationDetails() {
    const obj = {
      msg_id: 0,
      username: this.userName
    }
    this.service.post_data(this.notificationUrl, obj).subscribe((result) => {
      let json: any = this.service.parserToJSON(result);

      if (JSON.parse(json.string).length > 0) {
        if (JSON.parse(json.string)[0].img_url) {
          localStorage.setItem('notification_img_url', JSON.parse(json.string)[0].img_url)
          this.modalpop();
        }
      }
    })
  }
  async modalpop() {
    const modal = await this.modalController.create({
      component: AdvertisementModalpopPage,
      cssClass: 'my-custom-modal-class',
      backdropDismiss: false
    });
    return await modal.present();
  }
}
