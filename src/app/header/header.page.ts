import { Component, OnInit, } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { GlobalServicesService } from '../global-services.service';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-header',
  templateUrl: './header.page.html',
  styleUrls: ['./header.page.scss'],
})
export class HeaderPage implements OnInit {

  constructor(private menu: MenuController,
    public service: GlobalServicesService,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private navController: NavController) {
    this.userName = localStorage.getItem('username');
    this.subscription = this.service.getMessage().subscribe(message => {
      if (message.text == 'startNotificationCountFunction') {
        this.getnotificationsList();
      }
    });
    this.service.invoke.subscribe((res) => {
      if (res) {
        this.cityChange(res);
      }
    })
  }
  cityChange(city) {
    if (!this.city) {
      this.city = city;
      localStorage.setItem('location', this.city);
    }
  }
  ngOnInit() {
    this.userInfo();
    // this.getUserPosition();
    this.service.checkGPSPermission();
  }
  notificationUrl = 'getNotificationText';
  notificationUnreadCount;
  userName;
  subscription: Subscription;
  openMenu() {
    // this.menu.enable(true, 'first');
    this.menu.open();
  }


  //=====================get Login Details=====================
  loggedDetails: any = {};
  userInfo() {
    if (localStorage.getItem('username')) {
      let obj = {
        username: localStorage.getItem('username'),
      }
      this.service.post_data("getAlumniOthersInfo", obj)
        .subscribe((result) => {

          let json: any = this.service.parserToJSON(result);
          var parsedJSON = JSON.parse(json.string);

          this.loggedDetails = parsedJSON[0];

        }, (error) => {
          console.log(error);
        });
    }
  }



  //=====================Get Current Location=====================
  options;
  currentPos;
  latitude;
  longitude;
  city = localStorage.getItem('location');
  getUserPosition() {
    this.options = {
      enableHighAccuracy: false
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      this.currentPos = pos;
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;
      this.getAddress(this.latitude, this.longitude);

    }, (err: PositionError) => {
      console.log("error : " + err.message);
      ;
    })
  }

  // geocoder options
  nativeGeocoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  // get address using coordinates
  getAddress(lat, long) {

    this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
      .then((res: NativeGeocoderResult[]) => {
        var address = this.pretifyAddress(res[0]);
        var value = address.split(",");
        var count = value.length;
        this.city = value[count - 5];
      })
      .catch((error: any) => {
        alert('Error getting location' + error + JSON.stringify(error));
      });

  }

  // address
  pretifyAddress(addressObj) {
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length)
        address += obj[val] + ', ';
    }
    return address.slice(0, -2);
  }


  getnotificationsList() {
    const obj = {
      msg_id: 0,
      username: this.userName,
      rows_in_page: 0,
      pageid: 1
    }
    this.service.post_data(this.notificationUrl, obj).subscribe((result) => {
      let json: any = this.service.parserToJSON(result);
      let list = JSON.parse(json.string);
      if (list) {
        this.notificationUnreadCount = 0;
        if (list.Details.length > 0) {
          list.Details.forEach((element, i) => {
            if (!JSON.parse(String(element.read_sts).toLowerCase())) {
              this.notificationUnreadCount++;
            }
          });
        }
      }
    })
  }
  moveNotificationPage() {
    this.navController.navigateForward('/notification');
  }
}
