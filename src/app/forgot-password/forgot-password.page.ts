import { Component, OnInit } from '@angular/core';
import { AlertController, NavController} from '@ionic/angular';
import { GlobalServicesService } from '../global-services.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl , Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  resetPasswordFg: FormGroup;
  UrNmeExistForm: FormGroup;
  constructor(public alertController: AlertController, public navCtrl: NavController,
    public toastController: ToastController, public service: GlobalServicesService) {
  }
  
  ForgotPwdUrCheck: any;
  UrNmeExistCheck: any;
  cnfm_pwd: any;

  ngOnInit() {
    this.formInit();
    this.UrNmeExistFormInt();
  }

// =====================FORM INIT=====================
  formInit(){
    this.resetPasswordFg = new FormGroup({
      'user_name': new FormControl(null, Validators.required),
      'pwd': new FormControl(null, Validators.required),
      'cnfm_pwd': new FormControl(null, Validators.required)
    });
  }
  // =====================UrNmeExistForm INIT=====================
  UrNmeExistFormInt() {
    this.UrNmeExistForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
    });
  }

  passwordIsNotSame: any;
  forgotPwdForm = false;

  //ForgotPwdUerNameCheck 
  ForgotPwdUserNameCheck(){

    if (this.UrNmeExistForm.valid) {

      this.service.post_data("getUsrNameExist", this.UrNmeExistForm.value)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          this.UrNmeExistCheck = JSON.parse(json.string);
          if (this.UrNmeExistCheck[0].Status == "EXIST") {
            this.presentUrExistToast(this.UrNmeExistCheck[0].Message);
            this.forgotPwdForm = true;
            this.resetPasswordFg.get("user_name").setValue(this.UrNmeExistForm.value.username);
            // this.UpdatePwd();
          } 
          if (this.UrNmeExistCheck[0].Status == "NOTEXIST") {
            this.presentUrExistToast(this.UrNmeExistCheck[0].Message);
          }
        }, (error) => {
          console.log(error);
        });

    } else {

    }
  }

  UpdatePwd() {

    if (this.resetPasswordFg.valid) {

      if (this.resetPasswordFg.value.pwd == this.resetPasswordFg.value.cnfm_pwd) {
        this.service.post_data("updateUsernamePwd", this.resetPasswordFg.value)
          .subscribe((result) => {
            let json: any = this.service.parserToJSON(result);
            this.ForgotPwdUrCheck = JSON.parse(json.string);
            if (this.ForgotPwdUrCheck[0].Status == "SUCCESS") {
              this.presentToast(this.ForgotPwdUrCheck[0].Message);
              this.resetForm();
              this.navCtrl.navigateRoot(['/login']);
              // this.ForgotPwdUrCheckAlert(this.ForgotPwdUrCheck[0].Message);
            } else {
            }
          }, (error) => {
            console.log(error);
          });
      } else {
        this.presentToast('Password and confirmation password must be identical');
      }

    } else {

    }
  }

  async presentUrExistToast(message_content) {
    const toast = await this.toastController.create({
      message: message_content,
      cssClass: 'danger',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

  async presentToast(message_content) {
    const toast = await this.toastController.create({
      message: message_content,
      cssClass: 'danger',
      position: 'top',
      duration: 5000
    });
    toast.present();
  }

  // =====================PAGE FORWARD=====================
  async ForgotPwdUrCheckAlert(message_content) {
    const alert = await this.alertController.create({
      cssClass: 'alert-custom-class',
      message: message_content,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          cssClass: 'warning',
          handler: () => {
          }
        }
      ]
    });
    await alert.present();
  }

  // =====================RESET FORM=====================
  resetForm() {
    this.resetPasswordFg.reset();
    this.UrNmeExistForm.reset();
  }

  // =====================Confirm Password Validation=====================
  cpwdErrMessage: string;
  MatchPassword = () => {
    var passwordControl = this.resetPasswordFg.controls['pwd'].value;
    var confirmPasswordControl = this.resetPasswordFg.controls['cnfm_pwd'].value;

    if (passwordControl !== confirmPasswordControl) {
      this.cpwdErrMessage = "Password and confirmation password must be identical.";
    } else {
      this.cpwdErrMessage = undefined;
    }
  }
  // =====================End of Confirm Password Validation=====================

  // =====================HIDE PASSWORD=====================
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  cpasswordType: string = 'password';
  cpasswordIcon: string = 'eye-off';

  hideShowcPassword() {
    this.cpasswordType = this.cpasswordType === 'text' ? 'password' : 'text';
    this.cpasswordIcon = this.cpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
// =====================end of HIDE PASSWORD=====================

// =====================PAGE FORWARD=====================
  async forgotPasswordAlert() {
    const alert = await this.alertController.create({
      cssClass: 'alert-custom-class',
      message: 'Reset password link has been sent to your email.',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          cssClass: 'warning',
          handler: () => {
          }
        }
      ]
    });
    await alert.present();
  }

}
