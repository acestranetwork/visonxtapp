import { Component, OnInit, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { GlobalServicesService } from '../global-services.service';
import { MenuController, AlertController, NavController, PopoverController, Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { AddCategoryPage } from '../modal/add-category/add-category.page';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Keyboard } from '@ionic-native/keyboard';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { ModalController } from '@ionic/angular';
import { ToastController, ViewWillLeave } from '@ionic/angular';
import { File } from '@ionic-native/File/ngx';
import { CacheService } from "ionic-cache";
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray, Form } from '@angular/forms';
import { element } from 'protractor';

@Component({
  selector: 'app-category-selection',
  templateUrl: './category-selection.page.html',
  styleUrls: ['./category-selection.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CategorySelectionPage implements OnInit, ViewWillLeave {
  @ViewChild('videoElement', { static: false }) myVideo: ElementRef;
  testList: any = [
    { testID: 1, testName: 'Sports', checked: false },
    { testID: 2, testName: 'Culture', checked: false },
    { testID: 3, testName: 'Fashion Week', checked: false },
    { testID: 4, testName: "Candid", checked: false },
    { testID: 5, testName: "Sports", checked: false },
    { testID: 6, testName: "Culture", checked: false },
    { testID: 7, testName: "Fashion", checked: false },
    { testID: 8, testName: "Candid", checked: false },
    { testID: 9, testName: "Sports", checked: false },
    { testID: 10, testName: "Culture", checked: false },
    { testID: 11, testName: "Fashion", checked: false },
    { testID: 12, testName: "Candid", checked: false },
  ]

  //videoUrl = this.service.STREAMING_VIDEO_URL;
  mediaURL;
  selectedArray: any = [];
  constructor(private file: File,
    public service: GlobalServicesService,
    private menu: MenuController,
    public navCtrl: NavController,
    public popoverController: PopoverController,
    public alertController: AlertController,
    private router: Router,
    private route: ActivatedRoute,
    private webview: WebView,
    public modalController: ModalController,
    public toastController: ToastController,
    public cache: CacheService,
    private location: Location,
    public platform: Platform,
    private formBuilder: FormBuilder) {
  }
  address: string;
  latitude: string;
  longitude: string;
  type: string;
  fileInfo: any;
  page: string;
  newCategory: any;
  imgCategoryList: any;
  AddImgCategory: any;
  mediaPath;
  showToolbar: boolean = true;
  UploadFileList: any;
  posterImage = '';
  progressValue = 0.0;
  color = "";
  focused;
  activeTab;
  imageUploadForm: FormGroup;
  singleArrayUrl = "getImgCategory_SingleList";
  multiArrayUrl = "getImgCategory_MutipleList";
  rootList: any = [];
  level1List: any = [];
  level2List: any = [];
  level3List: any = [];
  dynamicFormFieldsList: any = [];
  breadCrumbFieldsArray: any = [];
  checkboxValidation: any = [];
  rootId;
  rootValue;

  setActive(e) {

  }
  ngOnInit() {

    this.route.params.subscribe((params) => {
      this.address = params['location'];
      this.latitude = params['latitude'];
      this.longitude = params['longitude'];
      this.type = params['type'];
      this.fileInfo = params['fileinfo'];
      this.page = params['page'];
      this.mediaPath = params['mediaPath'];
      // alert("line101" + this.address)
      // alert("line102" + this.mediaPath)

    });

    this.getRootList();
    Keyboard.onKeyboardShow().subscribe(() => {
      this.showToolbar = false;
    });

    Keyboard.onKeyboardHide().subscribe(() => {
      this.showToolbar = true;
    });
    // setInterval(() => {
    //   if (this.progressValue != 1.0) {
    //     this.progressValue = this.progressValue + 0.1;
    //   }
    // }, 1000)
    this.formInit();

  }

  ionViewWillEnter() {
    if (this.type == 'video') {
      if (localStorage.getItem('videoThumbnailPosterImage')) {
        this.posterImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(JSON.parse(localStorage.getItem('videoThumbnailPosterImage'))));
      }
      this.convertFileUritoBase64(this.mediaPath)
      let video = this.myVideo.nativeElement;
      this.mediaPath = this.service.checkDomSanitizerForIOS(this.mediaPath);
      // video.src = this.webview.convertFileSrc(this.mediaPath);
      if (this.platform.is('android')) {
        this.mediaPath = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(this.mediaPath));
        video.src = this.webview.convertFileSrc(this.mediaPath);
        video.load();
      }
    }
    else {
      if (this.mediaPath.indexOf("data:") !== -1) {
        this.mediaURL = this.mediaPath;
      } else {
        this.mediaURL = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(this.mediaPath));
      }
    }

    this.service.sendMessage('startNotificationCountFunction');
  }

  formInit() {
    this.imageUploadForm = this.formBuilder.group({
      rootId: new FormControl('1', Validators.required),
      img_categ: new FormControl('', Validators.required),
      level1Val: new FormControl('', Validators.required),
      level2Val: new FormControl('', Validators.required),
      level3Val: new FormControl('', Validators.required),
      level4Val: this.formBuilder.array([]),
      colorVal: new FormControl('', Validators.required),
      // swatch: new FormControl('', Validators.required),
      tags: new FormControl('')
    })
  }
  dynamicValues(): FormArray {
    return this.imageUploadForm.get("level4Val") as FormArray;
  }
  set() {
  }
  //category array list

  // getRootwithLevel1List() {
  //   this.service.post_data(this.multiArrayUrl, { parentid: 0 }).subscribe((result) => {
  //     let json: any = this.service.parserToJSON(result);
  //     let parsedJSON = JSON.parse(json.string);
  //     this.rootList = parsedJSON.category_list;
  //     this.level1List = parsedJSON.Womenswear;
  //   })
  // }

  getRootList() {
    this.service.post_data(this.singleArrayUrl, { parentid: 0 }).subscribe((result) => {
      let json: any = this.service.parserToJSON(result);
      let parsedJSON = JSON.parse(json.string);
      this.rootList = parsedJSON;
      this.activeTab = parsedJSON[0].sno;
      this.getLevel1List(parsedJSON[0].sno, parsedJSON[0].categ_name);
      this.progressBarAndBreadCrumb('root', parsedJSON[0].categ_name, 1);
    })
  }
  getLevel1List(id, name) {
    // if (this.activeTab != id) {
    this.activeTab = id;
    this.rootId = id;
    this.rootValue = name;
    this.imageUploadForm.get('rootId').patchValue(id);
    this.imageUploadForm.get('img_categ').patchValue(name);
    this.progressBarAndBreadCrumb('root', name, 1);
    this.service.post_data(this.singleArrayUrl, { parentid: this.imageUploadForm.get('rootId').value }).subscribe((result) => {
      this.formReset();
      let json: any = this.service.parserToJSON(result);
      let parsedJSON = JSON.parse(json.string);
      this.level1List = parsedJSON;
      this.getLevel2List();
    })
    // }
  }
  getLevel2List() {
    if (this.level1List.length != 0) {
      setTimeout(() => {
        this.service.post_data(this.singleArrayUrl, { parentid: this.imageUploadForm.get('level1Val').value }).subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          this.imageUploadForm.get('level2Val').reset();
          let parsedJSON = JSON.parse(json.string);
          this.level2List = parsedJSON;
          if (this.imageUploadForm.get('level1Val').value) {
            let val = this.level1List.filter(a => a.sno == this.imageUploadForm.get('level1Val').value)[0];
            this.progressBarAndBreadCrumb('level1Val', val.categ_name, 2);
          }
        })
      }, 500);
    } else {
      this.level2List = [];
      this.getLevel3List();
    }

  }
  getLevel3List() {
    if (this.level2List.length != 0) {
      setTimeout(() => {
        this.service.post_data(this.singleArrayUrl, { parentid: this.imageUploadForm.get('level2Val').value }).subscribe((result) => {
          this.imageUploadForm.get('level3Val').reset();
          let json: any = this.service.parserToJSON(result);
          let parsedJSON = JSON.parse(json.string);
          this.level3List = parsedJSON;
          if (this.imageUploadForm.get('level2Val').value) {
            let val = this.level2List.filter(a => a.sno == this.imageUploadForm.get('level2Val').value)[0];
            this.progressBarAndBreadCrumb('level2Val', val.categ_name, 3);
          }
        })
      }, 500);
    }
    else {
      this.level3List = [];
      this.getDynamicFormControlList();
    }
  }
  getDynamicFormControlList() {
    if (this.level3List.length != 0) {
      setTimeout(() => {
        this.service.post_data(this.multiArrayUrl, { parentid: this.imageUploadForm.get('level3Val').value }).subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          let FormArray = this.imageUploadForm.get('level4Val') as FormArray;
          this.dynamicValuesReset();
          if (JSON.parse(json.string).length != 0) {
            JSON.parse(json.string).forEach((element, i) => {
              element.categ_list.forEach(ele => {
                if (element.ctrl_type == 'chk') {
                  ele.status = false;
                }
              });
              let form: FormGroup = this.formBuilder.group({
                labelName: element.categ_name,
                labelValue: new FormControl('', Validators.required),
                categList: new Array(element.categ_list),
                ctrlType: element.ctrl_type
              })
              let obj = {
                borderColor: 'white'
              }
              this.checkboxValidation.push(obj)
              FormArray.push(form);
            });
          }
          this.dynamicFormFieldsList = JSON.parse(json.string);
          if (this.imageUploadForm.get('level3Val').value) {
            let val = this.level3List.filter(a => a.sno == this.imageUploadForm.get('level3Val').value)[0];
            this.progressBarAndBreadCrumb('level3Val', val.categ_name, 4);
          }
        })
      }, 500);
    } else {
      this.dynamicValuesReset();
    }
  }
  checkbox(i, j, v) {
    let FormArray = this.imageUploadForm.get('level4Val') as FormArray;
    let FormArray2 = FormArray.at(i).get('categList') as FormArray;
    FormArray2.value.forEach((element, a) => {
      if (j == a) {
        element.status = v;
      }
    });
    let ii = 0;
    let arr = [];
    FormArray2.value.forEach((element, b) => {
      if (element.status) {
        ii = 1;
        arr.push(element.categ_name);
      }
    });
    FormArray.at(i).get('labelValue').patchValue(arr);
    if (ii == 0) {
      this.checkboxValidation[i].borderColor = 'red'
    } else if (ii == 1) {
      this.checkboxValidation[i].borderColor = 'yellow'
    }
  }
  colorPickerSelect(e) {
    this.imageUploadForm.get('colorVal').patchValue(e);
    this.progressBarAndBreadCrumb('color', e, this.imageUploadForm.get('level4Val').value.length + 5);
  }

  progressBarAndBreadCrumb(label, val, i) {
    if (String(val) == 'Womenswear' || String(val) == 'Menswear' || String(val) == 'Kidswear') {
      this.breadCrumbFieldsArray = [];
    }
    let index = this.breadCrumbFieldsArray.findIndex(a => a.label == label);
    if (index > -1) {
      this.breadCrumbFieldsArray.splice(index, 1)
    }
    let validValues = 0;
    let Form = this.imageUploadForm.controls;
    let FormArray = this.imageUploadForm.get('level4Val') as FormArray;
    let objFields = Object.keys(Form).length - 1;
    let formArrayFields = FormArray.length;
    let totalFeildsLength = objFields + formArrayFields;
    Object.keys(Form).forEach((field, i) => {
      if (field != 'level4Val') {
        if (this.imageUploadForm.get(field).valid) {
          validValues++;
        }
      }
    });
    FormArray.controls.forEach((field, j) => {
      if (field.valid) {
        validValues++;
      }
    });
    this.progressValue = Math.round((validValues / totalFeildsLength) * 10) / 10;
    const obj = {
      label: label,
      val: val,
      id: i
    }
    this.breadCrumbFieldsArray.push(obj);
    this.breadCrumbFieldsArray.sort((a, b) => a.id - b.id);
    if (Number(i) == 1 || Number(i) == 2 || Number(i) == 3) {
      this.breadCrumbFieldsArray.splice(Number(i), this.breadCrumbFieldsArray.length)
    }
  }

  dynamicValuesReset() {
    this.color = '';
    this.imageUploadForm.get('colorVal').reset();
    this.imageUploadForm.get('tags').reset();
    this.checkboxValidation = [];
    let FormArray = this.imageUploadForm.get('level4Val') as FormArray;
    while (FormArray.length != 0) {
      FormArray.removeAt(0);
    }
  }
  formReset() {
    this.imageUploadForm.reset();
    this.dynamicValuesReset();
    if (this.rootId && this.rootValue) {
      this.imageUploadForm.get('rootId').patchValue(this.rootId);
      this.imageUploadForm.get('img_categ').patchValue(this.rootValue);
    }
  }


  // ===================Validation part===============
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        if (control.invalid) {
          control.markAsTouched({ onlySelf: true });
        }
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      } else if (control instanceof FormArray) {
        control.controls.forEach((element, i) => {
          this.validateAllFormFields(element as FormGroup);
        });
      }
    });
  }
  textareaMaxLengthValidation() {
    if (this.imageUploadForm.get('tags').value.length > 50) {
      this.imageUploadForm.get('tags').patchValue(this.imageUploadForm.get('tags').value.slice(0, 50))
    }
  }
  checkBoxValidation() {
    let FormArray = this.imageUploadForm.get('level4Val') as FormArray;
    if (FormArray.length > 0) {
      FormArray.value.forEach((element, a) => {
        if (element.ctrlType == 'chk') {
          let ii = 0;
          element.categList.forEach((ele, b) => {
            if (ele.status) {
              ii = 1;
            }
          });
          if (ii == 0) {
            this.checkboxValidation[a].borderColor = 'red';
          }
        }
      });
    }
  }
  // ==================



  async presentToast(message_content) {
    const toast = await this.toastController.create({
      message: message_content,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

  async presentAlert5(type, data) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      subHeader: type,
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }



  imgCategArray: any;

  // =====================UPLOAD FILE=====================

  async uploadFile() {
    // alert('formSTatus' + this.imageUploadForm.valid)
    if (this.imageUploadForm.valid) {
      // if (this.selectedArray.length == 0) {
      //   const alert = await this.alertController.create({
      //     cssClass: 'alert-custom-class',
      //     header: 'Error',
      //     message: 'Please Select the Any Category',
      //     backdropDismiss: false,
      //     buttons: [
      //       {
      //         text: 'OK',
      //         cssClass: 'warning',
      //       }
      //     ]
      //   });

      //   await alert.present();
      // } else {
      let username = localStorage.getItem('username');
      let stud_type = localStorage.getItem('stud_type');
      let filetype;
      let contentType;
      let base64String;
      let mediaType;
      let city;
      let latitude_longitude: any = [];
      let baseVale;
      let loc;
      let value = String(this.address).split(",");
      if (this.platform.is('ios')) {
        city = value[value.length - 2];
        loc = value[value.length - 2] + ',' + value[value.length - 1];
      }
      else {
        city = value[4];
        loc = value[3] + ',' + city;
      }
      if (this.platform.is('ios') && this.type == 'video') {
        baseVale = String(this.mediaPath).split(' ')[4];
      }
      else {
        baseVale = this.mediaPath;
      }
      // alert('line507 ---' + baseVale.indexOf("data:"))
      if (baseVale.indexOf("data:") !== -1) {
        mediaType = baseVale;
      } else {
        // alert("problem")
        mediaType = await this.service.imagetobase64(baseVale);
      }
      // alert('line510 ---' + mediaType)
      let splitted = mediaType.split(",");
      base64String = splitted[1].toString();
      if (this.type == 'image') {
        contentType = "image/*";
        filetype = "jpg";
      } else {
        if (this.platform.is('ios')) {
          contentType = "video/*";
          filetype = "MOV";
        } else {
          contentType = "video/*";
          filetype = "mp4";
        }
      }
      // alert('line525 ---' + base64String)
      let FormArray = this.imageUploadForm.get('level4Val') as FormArray;
      FormArray.value.forEach((element, i) => {
        delete element.categList;
      });
      // alert('line539 ---' + localStorage.getItem('videoThumbnailPosterImage'))
      // alert('line540 ---' + localStorage.getItem('videoThumbnailPosterImage') ? JSON.parse(localStorage.getItem('videoThumbnailPosterImage').replace('data:image/jpeg;base64,', '')) : '')

      // alert('line539 ---Ready')
      let obj = {
        'image_video': this.type == 'image' ? "IMAGE" : "VIDEO",
        'city': city,
        'location': loc,
        'latitude_longitude': '',
        'stud_type': stud_type,
        'username': username,
        // 'img_categ': '',
        'file_ext': filetype,
        'contentType': contentType,
        'base64String': base64String,
        'fileinfo': this.fileInfo,
        'base64StringThump': localStorage.getItem('videoThumbnailPosterImage') ? JSON.parse(localStorage.getItem('videoThumbnailPosterImage').replace('data:image/jpeg;base64,', '')) : ''
      };
      this.imageUploadForm.value.level4Val = JSON.stringify(this.imageUploadForm.value.level4Val)
      latitude_longitude.push(this.latitude);
      latitude_longitude.push(this.longitude);
      // let imgCategArrayOne = [];
      // this.selectedArray.forEach(function (value) {
      //   imgCategArrayOne.push(value.categ_name);
      // });
      // this.imgCategArray = imgCategArrayOne;
      // obj.img_categ = this.imgCategArray;
      obj.latitude_longitude = latitude_longitude;
      this.service.post_data("uploadFileBase64", Object.assign(obj, this.imageUploadForm.value))
        .subscribe((result) => {
          localStorage.removeItem('originalImg');
          localStorage.removeItem('editedFilters');
          localStorage.removeItem('videoFileSize');
          localStorage.removeItem('fileSize');
          localStorage.removeItem('videoThumbnailPosterImage');
          let json: any = this.service.parserToJSON(result);
          this.UploadFileList = JSON.parse(json.string);
          this.submitAlert(this.UploadFileList[0].Message);
        }, (error) => {
          console.log(error);
          alert(error)
        });

      // }
    }
    else {
      const alert = await this.alertController.create({
        cssClass: 'alert-custom-class-info',
        header: 'Info',
        message: 'Please fill all mandatory fields',
        backdropDismiss: false,
        buttons: [
          {
            text: 'OK',
            cssClass: 'warning',
          }
        ]
      });

      await alert.present();
      this.validateAllFormFields(this.imageUploadForm);
      this.checkBoxValidation();
    }
  }

  async submitAlert(message_content) {
    this.cache.clearAll();
    // if (this.selectedArray.length == 0) {

    //   const alert = await this.alertController.create({
    //     cssClass: 'alert-custom-class',
    //     header: 'Error',
    //     message: 'Please Select the Any Category',
    //     backdropDismiss: false,
    //     buttons: [
    //       {
    //         text: 'OK',
    //         cssClass: 'warning',
    //       }
    //     ]
    //   });

    //   await alert.present();
    // } else {

    const alert = await this.alertController.create({
      cssClass: 'alert-custom-class',
      message: message_content,
      backdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          cssClass: 'warning',
          handler: () => {
            this.navCtrl.navigateForward(['home']);
          }
        }
      ]
    });
    await alert.present();
    // }

  }

  openMenu() {
    this.menu.open();
  }


  goBack() {
    // if (this.page == 'home') {
    //   this.router.navigate(['/home', { 'type': this.type, 'page': "home" }]);
    // } else {
    // this.router.navigate(['/preview-filter', { 'type': this.type, 'page': "preview" }], );
    this.location.back();
    // }
  }

  //accodion
  isGroupShown: boolean;
  isCategoryShown: boolean;
  toggleCategoryIcon = 'add'
  toggleIcon = 'add';
  toggleGroup = function (value) {
    this.isGroupShown = value;
    this.toggleIcon = this.toggleIcon == 'add' ? 'remove' : 'add';
  };
  toggleCategory = function (value) {
    this.isCategoryShown = value;
    this.toggleCategoryIcon = this.toggleCategoryIcon == 'add' ? 'remove' : 'add';
  };

  async openViewer() {
    const modal = await this.modalController.create({
      component: ViewerModalComponent,
      componentProps: {
        src: this.webview.convertFileSrc(this.mediaPath),// required
        title: 'Photo Viewer', // optional
      },
      cssClass: 'ion-img-viewer', // required
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();
  }


  // =====================End of Category list=====================

  convertFileUritoBase64(ImagePath) {
    let filePath;
    var removePath = String(ImagePath).replace(' (see http://g.co/ng/security#xss)', '');
    var splitPath = removePath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    if (this.platform.is('ios') && ImagePath.indexOf("SafeValue") !== -1) {
      filePath = String(ImagePath.split(imageName)[0]).split(' ')[4];
      // alert('IOS----' + filePath);
    }
    else {
      filePath = ImagePath.split(imageName)[0];
    }
    // alert('line 686' + imageName + '-----' + filePath);

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      // alert('line 689' + base64);
      if (this.platform.is('ios')) {
        this.mediaPath = this.service.checkDomSanitizerForIOS(base64);
      }
      else {
        this.mediaPath = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(base64));
      }
    }, error => {
      console.log("CROP ERROR -> " + JSON.stringify(error));
      alert(error)
    }).catch((e) => {
      alert(e)
    });

  }


  ionViewWillLeave() {
    if (this.type != 'video') {
      this.service.changeMessage(this.mediaPath);
    }
  }


}
