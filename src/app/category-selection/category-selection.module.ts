import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategorySelectionPageRoutingModule } from './category-selection-routing.module';

import { CategorySelectionPage } from './category-selection.page';
import { HeaderPageModule } from '../header/header.module';
import { ColorPickerModule } from 'ngx-color-picker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    IonicModule,
    CategorySelectionPageRoutingModule, HeaderPageModule, ColorPickerModule,
  ],
  declarations: [CategorySelectionPage]
})
export class CategorySelectionPageModule { }
