import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, NavParams } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { GlobalServicesService } from '../global-services.service';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  input_password: any = 'password';
  signinFg: FormGroup;
  submitted: boolean;
  verifyList: any;
  loggedDetails: any;
  constructor(public alertController: AlertController,
    public navCtrl: NavController,
    private router: Router,
    private fb: FormBuilder,
    private webview: WebView,
    public service: GlobalServicesService,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private platform: Platform) {
  }

  ngOnInit() {
    this.formInit();
    this.getUserPosition();
  }

  // =====================FORM INIT=====================
  formInit() {
    this.signinFg = this.fb.group({
      username: ['', Validators.required],
      pwd: ['', Validators.required]
    });
  }

  // =====================SIGNIN PASSWORD=====================
  async signinFunction() {
    if (this.signinFg.valid) {
      this.service.post_data("validateStudLogin", this.signinFg.value)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          this.verifyList = JSON.parse(json.string);
          if (this.verifyList[0].Status == "FAIL") {
            this.signupAlert(this.verifyList[0].Message);
          } else {
            this.loggedDetails = this.verifyList[0];
            this.service.publishSomeData({
              user: this.loggedDetails.usr_name
            });
            localStorage.setItem('username', this.loggedDetails.usr_name);
            localStorage.setItem('mobileno', this.loggedDetails.mobileno);
            localStorage.setItem('stud_type', this.loggedDetails.trendspoter_type);
            localStorage.setItem('upload_image', this.loggedDetails.upload_image);
            // this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0]['OTP'], 'mobileno': obj.mobileno, 'data': JSON.stringify(data), 'page': 'signup' }]);
            // this.navCtrl.navigateRoot(['/home', { 'data': this.loggedDetails }]);
            this.sendOTP(this.verifyList[0].mobileno);
            this.service.callMenuFunction(this.city);
          }
        }, (error) => {
          console.log(error);
        });
    } else {
    }
  }

  // =====================Send OTP=====================

  sendOTP = (mobileno) => {
    let obj: any = {};
    obj.mobileno = mobileno;
    this.service.post_data_otp("sendOtpViaSMS", obj)
      .subscribe((result) => {
        let json: any = this.service.parserToJSON(result);
        var parsedJSON = JSON.parse(json.string);
        this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0].OTP, 'mobileno': this.loggedDetails.mobileno, 'data': this.loggedDetails, 'page': 'login' }]);
      }, (error) => {
        console.log(error);
      });
  }



  // =====================PAGE FORWARD=====================
  async signupAlert(message_content) {
    const alert = await this.alertController.create({
      cssClass: 'alert-custom-class',
      message: message_content,
      backdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          cssClass: 'warning',
          handler: () => {
          }
        }
      ]
    });
    await alert.present();
    this.signinFgReset();
  }

  // =====================HIDE PASSWORD=====================
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  signinFgReset() {
    this.signinFg.reset();
  }
  //========================location=========================
  options;
  currentPos;
  latitude;
  longitude;
  city;
  getUserPosition() {
    this.options = {
      enableHighAccuracy: false
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      this.currentPos = pos;
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;
      this.getAddress(this.latitude, this.longitude);
    }, (err: PositionError) => {
      console.log("error : " + err.message);
      ;
    })
  }

  // geocoder options
  nativeGeocoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  // get address using coordinates
  getAddress(lat, long) {
    this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
      .then((res: NativeGeocoderResult[]) => {
        var address = this.pretifyAddress(res[0]);
        var value = address.split(",");
        var count = value.length;
        // country = value[count - 1];
        // state = value[count - 2];
        if (this.platform.is('ios')) {
          this.city = value[value.length - 2];
        }
        else {
          this.city = value[4];
        }
        // alert(address);
        // alert(this.city);
      })
      .catch((error: any) => {
        alert('Error getting location' + error + JSON.stringify(error));
      });
  }

  // address
  pretifyAddress(addressObj) {
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length)
        address += obj[val] + ', ';
    }
    return address.slice(0, -2);
  }
}
