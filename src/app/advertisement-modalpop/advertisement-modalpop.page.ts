import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-advertisement-modalpop',
  templateUrl: './advertisement-modalpop.page.html',
  styleUrls: ['./advertisement-modalpop.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdvertisementModalpopPage implements OnInit, AfterViewInit {

  constructor(private modalController: ModalController) {
    this.userName = localStorage.getItem('username');
  }

  ngOnInit() {
    this.imageContent = localStorage.getItem('notification_img_url');
  }
  ngAfterViewInit() {
    // this.getnotificationContent();
  }
  notificationUrl = 'getNotificationImage';
  imageContent = '';
  userName;
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
    
    localStorage.removeItem('notification_img_url');
  }
}
