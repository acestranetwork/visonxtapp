import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdvertisementModalpopPage } from './advertisement-modalpop.page';

const routes: Routes = [
  {
    path: '',
    component: AdvertisementModalpopPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdvertisementModalpopPageRoutingModule {}
