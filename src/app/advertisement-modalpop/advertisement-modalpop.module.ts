import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdvertisementModalpopPageRoutingModule } from './advertisement-modalpop-routing.module';

import { AdvertisementModalpopPage } from './advertisement-modalpop.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdvertisementModalpopPageRoutingModule
  ],
  declarations: [AdvertisementModalpopPage],
  exports: [AdvertisementModalpopPage]
})
export class AdvertisementModalpopPageModule { }
