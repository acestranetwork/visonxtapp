import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'; 
import { IonicModule } from '@ionic/angular';
import { PreviewFilterPageRoutingModule } from './preview-filter-routing.module';
import { PreviewFilterPage } from './preview-filter.page';
//
import { HeaderPageModule } from '../header/header.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreviewFilterPageRoutingModule,HeaderPageModule
  ],
  declarations: [PreviewFilterPage]
})
export class PreviewFilterPageModule {}
