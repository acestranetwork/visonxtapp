import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { GlobalServicesService } from '../global-services.service';
import { ActionSheetController, MenuController, AlertController, NavController, PopoverController, Platform } from '@ionic/angular';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { stringify } from '@angular/compiler/src/util';
import { ImageCropModalPage } from '../image-crop-modal/image-crop-modal.page';
import { ModalController, ViewWillLeave } from '@ionic/angular';
import * as fx from 'glfx-es6';
import { VideoEditor, CreateThumbnailOptions } from '@ionic-native/video-editor';
@Component({
  selector: 'app-preview-filter',
  templateUrl: './preview-filter.page.html',
  styleUrls: ['./preview-filter.page.scss'],
})

export class PreviewFilterPage implements OnInit, AfterViewInit, ViewWillLeave {
  @ViewChild('imageSrc') imageSrc: ElementRef;
  @ViewChild('canvasElement') canvasElement: ElementRef;
  location: any;
  tempPath: string;
  page: string;
  fileInfo: any = {};
  originalImage: any;
  originalImg: any;
  enableEditMode: boolean = false;
  UploadMode: boolean = false;
  croppedImagepath = "";
  isLoading = false;
  originalVideo: any;
  imageTag;
  canvas: any;
  context: any;
  ratio: any;
  imageElem: any;
  texture: any;
  newImage: any;
  brightness: number = 0;
  contrast: number = 0;
  hue: number = 0;
  saturation: number = 0;
  unsharpMask: any = { radius: 100, strength: 2 };
  property: any;
  propertyTrue = false;
  type;
  posterImage = '';
  posterImageUrl;
  constructor(private file: File,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public service: GlobalServicesService,
    private menu: MenuController,
    public navCtrl: NavController,
    private route: ActivatedRoute,
    private router: Router,
    private androidPermissions: AndroidPermissions,
    private webview: WebView,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public platform: Platform,
    public modalController: ModalController,

  ) {
  }
  ngOnInit() {
    // =====================Check app permissions===================== 

    this.checkPermissions();
    // =====================Get params value===================== 
    this.route.params.subscribe((params) => {
      // alert(params['tempPath']);
      this.location = params['location'];
      this.type = params['type'];
      this.tempPath = params['tempPath'];
      this.page = params['page'];
      this.fileInfo = params['fileinfo']
      if (params['tempPath'] != undefined) {
        this.originalImg = params['tempPath'];
        this.originalImage = params['tempPath'];
        localStorage.setItem('originalImg', this.originalImg)
      } else if (localStorage.getItem('originalImg') != undefined) {
        this.originalImg = localStorage.getItem('originalImg');
        this.originalImage = localStorage.getItem('originalImg');
      }
    });
    this.getUserPosition();
  }

  ionViewWillEnter() {
    this.brightness = 0;
    this.contrast = 0;
    this.hue = 0;
    this.saturation = 0;

    if (this.type == 'video') {
      // this.originalImage = this.convertFileuritoBase64(this.tempPath);
      this.originalImage = this.service.checkDomSanitizerForIOS(this.tempPath);
      // if (localStorage.getItem('videoThumbnailPosterImage')) {
      //   this.posterImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(JSON.parse(localStorage.getItem('videoThumbnailPosterImage'))));
      // } else {
      let option: CreateThumbnailOptions = { fileUri: this.tempPath, width: this.fileInfo.width, height: this.fileInfo.height, atTime: 0, outputFileName: this.fileInfo.fileName, quality: 100 };
      VideoEditor.createThumbnail(option).then(res => {

        this.posterImageUrl = "file://" + res;
        this.convertFileuritoBase64("file://" + res)
        // let base64 = this.service.imagetobase64("file://" + res)
        // setTimeout(() => {
        //   base64.then((r) => {
        //     localStorage.setItem('videoThumbnailPosterImage', JSON.stringify(r));
        //   })
        // }, 1000);

        // localStorage.setItem('videoThumbnailPosterImage', res);
        this.posterImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(res));

      }, (error) => {
        console.log(error)
      })
      // }
    }
    else {
      this.service.currentMessage.subscribe(message => {
        if (message != '') {
          this.originalImage = message;
          this.convertFileuritoBase64(message);
        }
      })
      this.imageTag = this.imageSrc.nativeElement;
    }
    // else {
    //   this.originalImage = this.convertFileuritoBase64(this.imageTag.src); // convert to weburl
    // }

    this.service.sendMessage('startNotificationCountFunction');

  }
  ionViewWillLeave() {
    if (this.posterImageUrl) {
      this.file.resolveLocalFilesystemUrl(this.posterImageUrl).then((fileEntry) => {
        fileEntry.remove(() => {
        }, (error) => {
          console.log(error)
        });
      }).catch((e) => {
        console.log(e)
      })
    }
  }
  ngAfterViewInit() {
    if (this.type == 'video') {
      this.originalImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(this.tempPath))
      // this.convertFileuritoBase64(this.tempPath);
    }
  }

  // =====================Check app permissions===================== 
  checkPermissions() {
    this.androidPermissions.requestPermissions(
      [
        this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
        this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
      ]
    );
  }

  // =====================Edit the Image===================== 
  // When Sliders Values Change, Update the CSS Variables


  //  Select the filter property
  selectedFilterProperty(value) {
    this.propertyTrue = true;
    // alert(value);
    this.property = value;
    // alert(this.property);
  }
  // =====================Image Crop===================== 
  // Crop the image
  async cropImage(imgPath) {
    const modal = await this.modalController.create({
      component: ImageCropModalPage,
      backdropDismiss: false,
      componentProps: {
        imageBase64: imgPath
      },
    });

    await modal.present();


    const result = await modal.onWillDismiss();

    if (result.data && result.data.croppedImageBase64) {
      this.originalImage = result.data.croppedImageBase64;
      this.newImage = new Image();
      this.newImage.onload = () => {
        this.imageFilter(this.contrast, this.saturation, this.brightness, this.hue, false);
      };
      this.newImage.src = result.data.croppedImageBase64;
    }

    return null;
  }

  // Show and save the cropped image
  convertFileuritoBase64(ImagePath) {
    this.isLoading = true;
    if (ImagePath.indexOf("data:") !== -1) {
      if (this.type != 'video') {
        this.newImage = new Image();
        this.newImage.onload = () => {
          this.imageFilter(this.contrast, this.saturation, this.brightness, this.hue, false);
        };
        this.newImage.src = ImagePath;
        this.originalImage = ImagePath;
      }
    } else {
      var copyPath = ImagePath;
      var splitPath = copyPath.split('/');
      var imageName = splitPath[splitPath.length - 1];
      var filePath = ImagePath.split(imageName)[0];
      this.tempPath = ImagePath;
      // alert(ImagePath); 
      this.file.readAsDataURL(filePath, imageName).then(base64 => {
        if (this.type == 'video') {
          localStorage.setItem('videoThumbnailPosterImage', JSON.stringify(base64));
        } else {
          this.newImage = new Image();
          this.newImage.onload = () => {
            this.imageFilter(this.contrast, this.saturation, this.brightness, this.hue, false);
          };
          this.newImage.src = base64;
          this.originalImage = base64;
          // this.createCanvasElement(base64);
        }
      }, error => {
        console.log("CROP ERROR -> " + JSON.stringify(error));
      });
    }
  }

  imageFilter(contrast, saturate, brightness, hue, savefilter) {
    let contrastFilter = contrast / 100;
    let saturateFilter = saturate / 100;
    let brightnessFilter = brightness / 100;
    let hueFilter = hue / 100;

    this.contrast = contrast;
    this.saturation = saturate;
    this.brightness = brightness;
    this.hue = hue;
    try {
      this.canvas = fx.canvas();
    } catch (e) {
      alert(e);
      return;
    }

    this.imageElem = this.imageSrc.nativeElement;
    this.texture = this.canvas.texture(this.imageElem);
    this.canvas.draw(this.texture).update();
    this.canvas.setAttribute("style", "object-fit:contain;object-position:center;");
    let previousSibling = this.imageElem.previousSibling;

    while (previousSibling && previousSibling.nodeType !== 1) {
      previousSibling = previousSibling.previousSibling;
    }

    if (previousSibling) {
      previousSibling.parentNode.removeChild(previousSibling);
    }

    this.imageElem.parentNode.insertBefore(this.canvas, this.imageElem);
    /// filters applied to clean text
    this.canvas.draw(this.texture)
      .hueSaturation(hueFilter, saturateFilter)//grayscale
      .brightnessContrast(brightnessFilter, contrastFilter)
      .update();

    /// replace image src 
    if (savefilter == true) {
      this.originalImage = this.canvas.toDataURL("image/jpg", 1.0);
      this.brightness = 0;
      this.contrast = 0;
      this.hue = 0;
      this.saturation = 0;
    }
  }

  // Save the changes and convert to the image

  saveChanges() {
    this.propertyTrue = true;
    this.property = null;
    this.imageFilter(this.contrast, this.saturation, this.brightness, this.hue, true);
  }

  restoreImage() {
    if (this.originalImage) {
      this.imageTag.nativeElement.src = this.originalImage;
    }
  }

  // =====================Enable Image Edit ===================== 
  enableEdit() {
    this.enableEditMode = !this.enableEditMode;
  }

  // =====================Forward the video data to next page =====================  
  submitFileData() {

    let media;
    if (this.originalVideo) {
      media = this.originalVideo;
    } else {
      media = this.originalImage;
    }
  
    const fileinfo = JSON.parse(this.fileInfo)
    if (this.type == 'video') {
      this.router.navigate(['/category-selection', { 'fileinfo': JSON.stringify(fileinfo), 'location': this.location, 'latitude': this.latitude, 'longitude': this.longitude, 'mediaPath': media, 'type': this.type, 'page': this.page }]);
    } else {
      this.router.navigate(['/category-selection', { 'fileinfo': JSON.stringify(fileinfo), 'location': this.location, 'latitude': this.latitude, 'longitude': this.longitude, 'mediaPath': media, 'type': this.type, 'page': this.page }]);
    }
  }
  async presentAlert5(type, data) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      subHeader: type,
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }
  //=====================Open Menu=====================
  openMenu() {
    this.menu.open();
  }

  //=====================Goto Back=====================
  goBack() {
    if (this.type != 'video') {
      this.imageElem = this.imageSrc.nativeElement;
      let previousSibling = this.imageElem.previousSibling;
      while (previousSibling && previousSibling.nodeType !== 1) {
        previousSibling = previousSibling.previousSibling;
      }
      if (previousSibling) {
        previousSibling.parentNode.removeChild(previousSibling);
      }
    }
    this.originalImage = "";
    this.property = '';
    this.type = '';
    this.navCtrl.back();
  }
  //=====================Get Current Location=====================
  options: GeolocationOptions;
  currentPos: Geoposition;
  locationTraces = [];
  latitude: any = 0; //latitude
  longitude: any = 0; //longitude
  address: string;
  getUserPosition() {
    this.options = {
      enableHighAccuracy: false
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      this.currentPos = pos;
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;

      this.getAddress(this.latitude, this.longitude);
    }, (err: PositionError) => {
      console.log("error : " + err.message);
      ;
    })
  }

  // geocoder options
  nativeGeocoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  // get address using coordinates
  getAddress(lat, long) {
    this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
      .then((res: NativeGeocoderResult[]) => {
        this.address = this.pretifyAddress(res[0]);
        this.location = this.pretifyAddress(res[0]);
      })
      .catch((error: any) => {
        alert('Error getting location' + error + JSON.stringify(error));
      });
  }

  // address
  pretifyAddress(addressObj) {
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length)
        address += obj[val] + ', ';
    }
    return address.slice(0, -2);
  }

}
