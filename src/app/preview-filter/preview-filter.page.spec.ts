import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PreviewFilterPage } from './preview-filter.page';

describe('PreviewFilterPage', () => {
  let component: PreviewFilterPage;
  let fixture: ComponentFixture<PreviewFilterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewFilterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PreviewFilterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
