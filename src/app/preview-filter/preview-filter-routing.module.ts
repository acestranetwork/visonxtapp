import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PreviewFilterPage } from './preview-filter.page';

const routes: Routes = [
  {
    path: '',
    component: PreviewFilterPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreviewFilterPageRoutingModule {}
