import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AluminisignupPage } from './aluminisignup.page';

const routes: Routes = [
  {
    path: '',
    component: AluminisignupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AluminisignupPageRoutingModule {}
