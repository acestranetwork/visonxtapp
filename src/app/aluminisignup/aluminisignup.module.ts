import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AluminisignupPageRoutingModule } from './aluminisignup-routing.module';

import { AluminisignupPage } from './aluminisignup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AluminisignupPageRoutingModule
  ],
  declarations: [AluminisignupPage]
})
export class AluminisignupPageModule {}
