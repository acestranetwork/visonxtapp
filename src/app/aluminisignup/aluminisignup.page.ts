import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NavController, ActionSheetController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { GlobalServicesService } from '../global-services.service';

import { Base64 } from '@ionic-native/base64/ngx';
@Component({
  selector: 'app-aluminisignup',
  templateUrl: './aluminisignup.page.html',
  styleUrls: ['./aluminisignup.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AluminisignupPage implements OnInit {

  currentImage: any;
  Images: any;
  imagesService: any;
  signupFg: FormGroup;
  inValidGender = false;
  degList: any;
  deviceId;
  devicePlatform;
  constructor(public navCtrl: NavController, private camera: Camera, public actionSheetCtrl: ActionSheetController, private fb: FormBuilder, private webview: WebView, public service: GlobalServicesService, public alertController: AlertController, public base64: Base64) {
  }


  ngOnInit() {

    if (localStorage.getItem('deviceId')) {
      this.deviceId = localStorage.getItem('deviceId')
    }
    if (localStorage.getItem('platform')) {
      this.devicePlatform = localStorage.getItem('platform')
    }
    this.formInit();
    this.getCampusList();
    this.getDegreeList();
  }


  // =====================FORM INIT=====================
  formInit() {
    this.signupFg = this.fb.group({
      stud_name: ['', Validators.required],
      name_of_deg: ['', Validators.required],
      year_of_comp: ['', Validators.required],
      campus: ['', Validators.required],
      dob_ddmmyyyy: ['', Validators.required],
      gender: ['', Validators.required],
      emailid: ['', { validators: [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")], updateOn: 'blur' }],
      mobileno: ['', Validators.required],
      interests: ['', Validators.required],
      hobby: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      pincode: ['', Validators.required],
      designation: ['', Validators.required],
      company: ['', Validators.required],
      file_ext: ['', Validators.required],
      contentType: ['', Validators.required],
      base64String: ['', Validators.required],
      linkedin_id: [''],
      fb_id: [''],
      instagram_id: [''],
      device_id: [this.deviceId],
      device: [this.devicePlatform],
      trend_spotter_type: 'NIFT-ALUMNI',
      username: ['', Validators.required],
      pwd: ['', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,15}$")]],
      confirmpassword: ['', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,15}$")]],
    });
  }

  // =====================GENDER VALIDATION=====================
  genderList = [
    {
      name: 'male',
      isSelected: false
    },
    {
      name: 'female',
      isSelected: false
    },
    {
      name: 'Others',
      isSelected: false
    }
  ];

  selectGender(gender) {
    this.genderList.forEach(g => {
      if (g.name === gender.name) {
        g.isSelected = !g.isSelected;
        this.inValidGender = false;
      } else {
        g.isSelected = false;
      }
    });
  }

  // =====================SIGN IN=====================
  signupFunction() {
    const data = this.signupFg.value;
    data[`gender`] = this.genderList.filter(gender => gender.isSelected).map(g => g.name)[0];
    if (data.gender != null) {
      this.inValidGender = false;
      this.signupFg.controls['gender'].setValue(data.gender);
    } else {
      this.inValidGender = true;
    }
    if (this.signupFg.valid) {
      let obj: any = {};
      obj.mobileno = this.signupFg.controls['mobileno'].value;
      this.service.post_data_otp("sendOtpViaSMS", obj)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          var parsedJSON = JSON.parse(json.string);
          this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0]['OTP'], 'mobileno': obj.mobileno, 'data': JSON.stringify(data), 'page': 'signup' }]);
        }, (error) => {
          console.log(error);
        });
    } else if (!this.signupFg.get('base64String').valid || !this.signupFg.get('file_ext').valid || !this.signupFg.get('contentType').valid) {
      this.alertMessage('Upload Image to Proceed Further');
    } else {
      this.validateAllFormFields(this.signupFg)
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        if (!control.valid) {
          control.markAsTouched({ onlySelf: true })
        }
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  // =====================UPLOAD PROFILE IMAGE=====================

  async uploadImage() {
    let actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Take photo',
          role: 'destructive',
          icon: 'camera',
          handler: () => {
            this.captureImage('camera');
          }
        },
        {
          text: 'Choose photo from Gallery',
          icon: 'images',
          handler: () => {
            this.captureImage('gallery');
          }
        },
      ]
    });
    actionSheet.present();
  }
  //CAPTURE IMAGE FUNCTION
  captureImage(data: any) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    if (data == 'camera') {
      options.sourceType = this.camera.PictureSourceType.CAMERA;
      this.camera.getPicture(options).then((imageData) => {
        this.currentImage = 'data:image/jpeg;base64,' + imageData;
        this.uploadFile(imageData);
      }, (err) => {
        this.imagesService.create(err, 'top', 'error');
        // Handle error
      });
    } else {
      if (data == 'gallery') {
        options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.camera.getPicture(options).then((imageData) => {
          this.currentImage = 'data:image/jpeg;base64,' + imageData;
          this.uploadFile(imageData);
        }, (err) => {
          // Handle error
          this.imagesService.create(err, 'top', 'error');
        });
      }
    }
  }

  //SUBMIT IMAGE DATA TO SERVER
  async uploadFile(imageData) {

    this.signupFg.controls['base64String'].setValue(imageData);
    this.signupFg.controls['contentType'].setValue("image/jpeg");
    this.signupFg.controls['file_ext'].setValue("jpeg");
  }



  // =====================End of UPLOAD PROFILE IMAGE=====================


  // =====================Get Degree list=====================

  getDegreeList = () => {
    let obj = {};
    this.service.post_data("getDegreeList", obj)
      .subscribe((result) => {
        let json: any = this.service.parserToJSON(result);
        this.degList = JSON.parse(json.string);
      }, (error) => {
        console.log(error);
      });
  }
  // =====================End of Degree list=====================

  // =====================Get Campus list=====================
  campusList: any;
  getCampusList = () => {
    let obj = {};
    this.service.post_data("getCampusList", obj)
      .subscribe((result) => {
        let json: any = this.service.parserToJSON(result);
        this.campusList = JSON.parse(json.string);
      }, (error) => {
        console.log(error);
      });
  }
  // =====================End of Degree list=====================

  // =====================Email Unique Validation=====================
  emailidErrMessage: string;
  validateEmailUnique = () => {
    const regex = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$");
    const valid = regex.test(this.signupFg.controls['emailid'].value);
    if (valid) {
      let obj: any = {};
      obj.emailid = this.signupFg.controls['emailid'].value;
      this.service.post_data("getEmailExist", obj)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          var parsedJSON = JSON.parse(json.string);
          if (parsedJSON[0].Status != "NOTEXIST") {
            this.emailidErrMessage = parsedJSON[0].Message;
            this.signupFg.controls['emailid'].setValue(undefined);
          } else {
            this.emailidErrMessage = undefined;
          }
        }, (error) => {
          console.log(error);
        });
    }

  }
  // =====================End of Email Unique Validation=====================

  // =====================Mobile Number Unique Validation=====================
  mobilenoErrMessage: string;
  validateMobileUnique = () => {
    if (this.signupFg.controls['mobileno'].value != null || this.signupFg.controls['mobileno'].value != undefined) {
      var monbilnoLength = (this.signupFg.controls['mobileno'].value).toString().length;
      if (monbilnoLength > 1 && monbilnoLength == 10) {
        this.mobilenoErrMessage = "";
        let obj: any = {};
        obj.mobno = this.signupFg.controls['mobileno'].value
        this.service.post_data("getMobileNoExist", obj)
          .subscribe((result) => {
            let json: any = this.service.parserToJSON(result);
            var parsedJSON = JSON.parse(json.string);
            if (parsedJSON[0].Status != "NOTEXIST") {
              this.mobilenoErrMessage = parsedJSON[0].Message;
              this.signupFg.controls['mobileno'].setValue(undefined);
            } else {
              this.mobilenoErrMessage = undefined;
            }
          }, (error) => {
            console.log(error);
          });
      } else {
        this.mobilenoErrMessage = "Mobile Number is Invalid.";
      }
    } else {
      this.mobilenoErrMessage = undefined;
    }


  }
  // =====================End of Mobile Number Unique Validation=====================

  // =====================Email Unique Validation=====================
  usernameErrMessage: string;
  validateUsernameUnique = () => {
    let obj: any = {};
    obj.username = this.signupFg.controls['username'].value;
    this.service.post_data("getUsrNameExist", obj)
      .subscribe((result) => {
        let json: any = this.service.parserToJSON(result);
        var parsedJSON = [];
        parsedJSON = JSON.parse(json.string);
        if (parsedJSON[0].Status != "NOTEXIST") {
          this.usernameErrMessage = parsedJSON[0].Message;
          this.signupFg.controls['username'].setValue(undefined);
        } else {
          this.usernameErrMessage = undefined;
        }

      }, (error) => {
        console.log(error);
      });
  }
  // =====================End of Email Unique Validation=====================
  // =====================Confirm Password Validation=====================
  cpwdErrMessage: string;
  MatchPassword = () => {
    var passwordControl = this.signupFg.controls['pwd'].value;
    var confirmPasswordControl = this.signupFg.controls['confirmpassword'].value;

    if (passwordControl !== confirmPasswordControl) {
      this.cpwdErrMessage = "Password and Confirm Password fields should match.";
    } else {
      this.cpwdErrMessage = undefined;
    }
  }
  // =====================End of Confirm Password Validation=====================

  // =====================HIDE PASSWORD=====================
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  cpasswordType: string = 'password';
  cpasswordIcon: string = 'eye-off';

  hideShowcPassword() {
    this.cpasswordType = this.cpasswordType === 'text' ? 'password' : 'text';
    this.cpasswordIcon = this.cpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  // =====================end of HIDE PASSWORD=====================
  // =====================Send OTP=====================

  sendOTP = (mobileno) => {
    let obj: any = {};
    obj.mobileno = mobileno;
    this.service.post_data_otp("sendOtpViaSMS", obj)
      .subscribe((result) => {
        let json: any = this.service.parserToJSON(result);
        var parsedJSON = JSON.parse(json.string);
        this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0]['OTP'], 'mobileno': mobileno }]);
      }, (error) => {
        console.log(error);
      });
  }


  async alertMessage(message) {
    const alert = await this.alertController.create({
      cssClass: 'alert-custom-class',
      message: message,
      buttons: [{
        text: 'OK',
        cssClass: 'warning',
      },]
    });
    await alert.present();
  }

  // =====================End of Degree list=====================



}
