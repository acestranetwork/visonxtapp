import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AluminisignupPage } from './aluminisignup.page';

describe('AluminisignupPage', () => {
  let component: AluminisignupPage;
  let fixture: ComponentFixture<AluminisignupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AluminisignupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AluminisignupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
