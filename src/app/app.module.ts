import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule, DomSanitizer, SafeResourceUrl, HammerModule } from '@angular/platform-browser';

import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { FileUploadModule } from 'ng2-file-upload';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Camera } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { File } from '@ionic-native/file/ngx';
import { Storage, IonicStorageModule } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { CountdownModule } from 'ngx-countdown';
import { WebView } from '@ionic-native/ionic-webview/ngx';
// import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { Base64 } from '@ionic-native/base64/ngx';
import { Device } from '@ionic-native/device/ngx';
import { LoaderService } from './shared/loader.service';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { CacheModule } from "ionic-cache";

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],

  imports: [
    BrowserModule, IonicModule.forRoot(),
    AppRoutingModule,
    FileUploadModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    CountdownModule,
    NgxIonicImageViewerModule,
    CacheModule.forRoot(),
    HammerModule
  ],

  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    Geolocation,
    StreamingMedia,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderService,
      multi: true
    },
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy
    },
    Device, MediaCapture, File, AndroidPermissions, WebView, NativeGeocoder, Base64, LocationAccuracy
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent],


})
export class AppModule { }
