import { Component, OnInit } from '@angular/core';
import { GlobalServicesService } from '../global-services.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  City: any;

  constructor(public service: GlobalServicesService) {
    this.City = localStorage.getItem('location');
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.userInfo();
  }

  //=====================Edit Profile=====================
  LinkedInEdit: boolean = false;
  instaEdit: boolean = false;
  fbEdit: boolean = false;
  editProfile(type) {

    if (type == 'linkedin') {
      this.LinkedInEdit = true;
    }

    if (type == 'insta') {
      this.instaEdit = true;
    }

    if (type == 'fb') {
      this.fbEdit = true;
    }

  }
  //=====================Save Profile=====================
  saveProfile(type, value) {
    let obj = {};

    if (type == 'linkedin') {
      this.LinkedInEdit = false;
      obj = {
        user_name: localStorage.getItem('username'),
        linkedin_id: value,
        fb_id: this.loggedDetails.fb_id,
        instagram_id: this.loggedDetails.insta_id,
      }
    }

    if (type == 'insta') {
      this.instaEdit = false;
      obj = {
        user_name: localStorage.getItem('username'),
        linkedin_id: this.loggedDetails.linkedin_id,
        fb_id: this.loggedDetails.fb_id,
        instagram_id: value,
      }
    }

    if (type == 'fb') {
      this.fbEdit = false;
      obj = {
        user_name: localStorage.getItem('username'),
        linkedin_id: this.loggedDetails.linkedin_id,
        fb_id: value,
        instagram_id: this.loggedDetails.insta_id,
      }
    }

    if (localStorage.getItem('username')) {
      this.service.post_data("updateSocialMedia", obj)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          var parsedJSON = JSON.parse(json.string);
          this.userInfo();
        }, (error) => {
          console.log(error);
        });
    }

  }

  //=====================get Login Details=====================
  loggedDetails: any = {};
  age;
  userInfo() {
    if (localStorage.getItem('username')) {
      let obj = {
        username: localStorage.getItem('username'),
      }
      this.service.post_data("getAlumniOthersInfo", obj)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          var parsedJSON = JSON.parse(json.string);
          this.loggedDetails = parsedJSON[0];
          var timeDiff = Math.abs(Date.now() - new Date(this.loggedDetails.dob).getTime());
          this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
        }, (error) => {
          console.log(error);
        });
    }
  }

}
