import { Component, OnInit, ViewChild } from '@angular/core';
import { CountdownComponent } from 'ngx-countdown';
import { NavController, ActionSheetController, AlertController, Platform } from '@ionic/angular';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { GlobalServicesService } from '../global-services.service';
import { ToastController } from '@ionic/angular';
import { Location } from '@angular/common';
declare var SMSReceive: any;
@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {
  @ViewChild('countdown', { static: false }) private countdown: CountdownComponent;
  interval: any;
  isClick = false;
  timeLeft = 45;
  color: any;
  enteredOTP;
  OTP: any = {
    first: '',
    second: '',
    third: '',
    fourth: '',
    fifth: ''
  };
  config = {
    format: 'mm:ss',
    leftTime: 45
  }
  otpDisabled: boolean = false;
  otpReceived;
  mobileno;
  page;
  data: any;
  subscription: any;
  constructor
    (public navCtrl: NavController,
      public service: GlobalServicesService,
      private route: ActivatedRoute,
      private router: Router,
      public toastController: ToastController,
      public platform: Platform,
      private location: Location
    ) {

  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.otpReceived = params['otp'];
      this.mobileno = params['mobileno'];
      this.page = params['page'];
      this.data = params['data'];
      // this.enteredOTP = params['otp'];
    });
  }


  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
      // do nothing
    })
    this.smsReadStart();
  }




  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  // =====================read SMS plugin for OTP Verification=====================
  // OTP: string = '';
  showOTPInput: boolean = false;
  OTPmessage: string = 'An OTP is sent to your number. You should receive it in 15 s'
  async presentToastOTP(message, position, duration) {
    const toast = await this.toastController.create({
      message: message,
      position: position,
      duration: duration
    });
    toast.present();
  }

  // =====================OTP Controller to move next=====================
  otpController(event, next, prev, index) {
    if (index == 6) {
    }
    if (event.target.value.length < 1 && prev) {
      prev.setFocus()
    }
    else if (next && event.target.value.length > 0) {
      next.setFocus();
    }
    else {
      return 0;
    }
  }

  // =====================Mobile Number  Validation=====================
  validateOTP = (mobileno) => {

    let obj: any = {};
    obj.mobileno = mobileno;
    obj.otp = Object.values(this.OTP).join('');
    if (Object.values(this.OTP).length == 5) {
      this.service.post_data_otp("ValidateOtp", obj)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          var parsedJSON = JSON.parse(json.string);
          if (parsedJSON[0]['Status'] == "SUCCESS") {
            if (this.page == 'signup') {
              this.navCtrl.navigateRoot(['/terms', { 'data': this.data }]);
            }
            if (this.page == 'login') {
              this.navCtrl.navigateRoot(['/home']);
              localStorage.setItem('login_token', '5$/7@nift)8G63');
            }
          } else {
            this.presentToastOTP(parsedJSON[0]['Message'], 'top', 1500);
          }
        }, (error) => {
          console.log(error);
        });
    } else {
      this.presentToastOTP('OTP is invalid', 'top', 1500);
    }

  }
  // =====================End of Mobile Number Unique Validation=====================

  // =====================Resend OTP=====================
  rsendOTP(m) {
    let obj: any = {};
    obj.mobileno = this.mobileno; //mobileno;
    this.service.post_data_otp("ReSendOtpViaSMS", obj)
      .subscribe((result) => {
        let json: any = this.service.parserToJSON(result);
        var parsedJSON = JSON.parse(json.string)[0];
        if (parsedJSON.Status == 'SUCCESS') {
          this.presentToast(parsedJSON.Message);
          this.isClick = false;
          this.otpDisabled = false;
          this.smsReadStart();
          this.timeLeft = 45;
        }
      }, (error) => {
        console.log(error);
      });
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }


  // =====================OTP Expired (Timer)=====================
  handleEvent(e) {
    if (e.action == "done") {
      this.enteredOTP = '';
      this.isClick = true;
      this.OTP = {};
      this.otpDisabled = true;
      this.color = '#1e2023';
      this.config.leftTime = 45;
      this.countdown.begin();
    }
  }


  smsReadStart() {
    SMSReceive.startWatch(() => {
      document.addEventListener('onSMSArrive', (args: any) => {
        if (args) {
          if (args.data.body && args.data.body.indexOf('VisioNxt') == 5) {
            let messageArray = args.data.body.split(' ');
            // this.enteredOTP = messageArray[5];
            this.patchValue(messageArray[5])
            this.smsReadStop();
          }
        }
      });
    }, (error) => {
      console.warn(error);
    });
  }

  smsReadStop() {
    SMSReceive.stopWatch(() => {
    }, (error) => {
      console.warn(error);
    });
  }


  patchValue(val) {
    let splitVal: Array<String> = val.split('');
    this.OTP.first = splitVal[0];
    this.OTP.second = splitVal[1];
    this.OTP.third = splitVal[2];
    this.OTP.fourth = splitVal[3];
    this.OTP.fifth = splitVal[4];
  }
  goBack() {
    this.location.back();
  }
}
