import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AlertController, NavController, ModalController, ViewWillEnter } from '@ionic/angular';
import { GlobalServicesService } from '../global-services.service';
@Component({
  selector: 'app-coverpage',
  templateUrl: './coverpage.page.html',
  styleUrls: ['./coverpage.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CoverpagePage implements OnInit, ViewWillEnter {

  constructor(public alertController: AlertController,
    public navCtrl: NavController,
    public modalController: ModalController,
    private service: GlobalServicesService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // this.service.checkGPSPermission();
    this.service.sendMessage('startNotificationCountFunction');
  }
  ionViewWillLeave() {
    // this.service.checkGPSPermission();
  }
  // =====================PAGE FORWARD=====================
  async signupAlert() {
    const alert = await this.alertController.create({

      cssClass: 'alert-custom-class1',

      // message: '<a class="close">&times;</a> <br><br> <img src="../../assets/image/logo.png" alt="g-maps" style="border-radius: 2px">',
      message: 'Are you an alumni of NIFT?',
      backdropDismiss: false,
      header: '',
      subHeader: '* NIFT students can sign in using CMS credentials. No need to sign up.',
      buttons: [
        {
          text: 'Login',
          cssClass: 'Login',
          handler: () => {
            this.navCtrl.navigateForward(['login']);
          }
        },
        {
          text: 'Yes',
          cssClass: 'warning',
          handler: () => {
            this.navCtrl.navigateForward(['aluminisignup']);
          }
        },
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.navCtrl.navigateForward(['niftsignup']);
          }
        },
        {
          text: 'Dismiss',
          role: 'dismiss',
          cssClass: 'light',
          handler: () => {
          }
        }
      ],
    });
    await alert.present();
  }

}
