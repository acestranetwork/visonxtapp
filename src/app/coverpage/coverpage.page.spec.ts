import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoverpagePage } from './coverpage.page';
describe('CoverpagePage', () => {
  let component: CoverpagePage;
  let fixture: ComponentFixture<CoverpagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverpagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoverpagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
