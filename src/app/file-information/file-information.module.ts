import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FileInformationPageRoutingModule } from './file-information-routing.module';

import { FileInformationPage } from './file-information.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FileInformationPageRoutingModule
  ],
  declarations: [FileInformationPage]
})
export class FileInformationPageModule {}
