import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FileInformationPage } from './file-information.page';

const routes: Routes = [
  {
    path: '',
    component: FileInformationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FileInformationPageRoutingModule {}
