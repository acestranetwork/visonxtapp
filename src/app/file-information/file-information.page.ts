import { Component, OnInit, Input  } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-file-information',
  templateUrl: './file-information.page.html',
  styleUrls: ['./file-information.page.scss'],
})
export class FileInformationPage implements OnInit {

  @Input() fileinfo: any = {};
  @Input() data: any = {};
  @Input() latitude: any;
  @Input() longitude: any;

  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
