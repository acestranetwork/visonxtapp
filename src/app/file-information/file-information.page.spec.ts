import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FileInformationPage } from './file-information.page';

describe('FileInformationPage', () => {
  let component: FileInformationPage;
  let fixture: ComponentFixture<FileInformationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileInformationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FileInformationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
