import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NiftsignupPage } from './niftsignup.page';

describe('NiftsignupPage', () => {
  let component: NiftsignupPage;
  let fixture: ComponentFixture<NiftsignupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NiftsignupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NiftsignupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
