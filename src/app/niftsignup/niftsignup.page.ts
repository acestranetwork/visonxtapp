import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActionSheetController, NavController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { GlobalServicesService } from '../global-services.service';
import { Base64 } from '@ionic-native/base64/ngx';
import { File } from '@ionic-native/file/ngx';
@Component({
  selector: 'app-niftsignup',
  templateUrl: './niftsignup.page.html',
  styleUrls: ['./niftsignup.page.scss'],
})
export class NiftsignupPage implements OnInit {

  constructor(private camera: Camera,
    public navCtrl: NavController,
    public actionSheetCtrl: ActionSheetController,
    private fb: FormBuilder,
    private webview: WebView,
    private Service: GlobalServicesService,
    private file: File,
    private alertController: AlertController) {

  }
  deviceId;
  devicePlatform;
  ngOnInit() {
    if (localStorage.getItem('deviceId')) {
      this.deviceId = localStorage.getItem('deviceId')
    }
    if (localStorage.getItem('platform')) {
      this.devicePlatform = localStorage.getItem('platform')
    }
    this.formInit();
  }

  // ===============Variables===================
  imgURL: any;
  Images: any;
  service: any;
  currentImage: string;
  signupFg: FormGroup;
  inValidGender = false;
  url = 'studRegisBase64';
  mobileVerifyUrl = 'getMobileNoExist';
  mailVerfyUrl = 'getEmailExist';
  mobilenoErrMessage: string;
  emailidErrMessage: string;
  usernameErrMessage: string;
  cpwdErrMessage: string;
  imageUrlPath;
  genderList = [
    {
      name: 'male',
      isSelected: false
    },
    {
      name: 'female',
      isSelected: false
    },
    {
      name: 'Others',
      isSelected: false
    }
  ];
  // ========================================

  // =====================FORM INIT=====================
  formInit() {
    this.signupFg = this.fb.group({
      trend_spotter_type: 'OTHERS',
      stud_name: ['', Validators.required],
      name_of_deg: ['', Validators.required],
      year_of_comp: ['', Validators.required],
      campus: ['', Validators.required],
      dob_ddmmyyyy: ['', Validators.required],
      gender: ['', Validators.required],
      emailid: ['', { validators: [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")] }],
      mobileno: ['', [Validators.required, Validators.min(1000000000), Validators.max(9999999999)]],
      interests: ['', Validators.required],
      hobby: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      pinCode: ['', Validators.required],
      designation: ['', Validators.required],
      company: ['', Validators.required],
      username: ['', Validators.required],
      pwd: ['', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,15}$")]],
      confirmpassword: ['', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,15}$")]],
      linkedin_id: [''],
      fb_id: [''],
      instagram_id: [''],
      device_id: [this.deviceId],
      device: [this.devicePlatform],
      file_ext: ['', Validators.required],
      contentType: ['', Validators.required],
      base64String: ['', Validators.required],
    });
  }
  // ==================

  // =====================VALIDATION=====================
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        if (!control.valid) {
          control.markAsTouched({ onlySelf: true })
        }
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  selectGender(gender) {
    this.genderList.forEach(g => {
      if (g.name === gender.name) {
        g.isSelected = !g.isSelected;
        this.signupFg.get('gender').patchValue(g.name);
        this.inValidGender = false;
      } else {
        g.isSelected = false;
      }
    });
  }

  mobileNumberVerification() {
    if (this.signupFg.get('mobileno').valid) {
      this.mobilenoErrMessage = '';
      const obj = {
        mobno: this.signupFg.get('mobileno').value
      };
      this.Service.post_data(this.mobileVerifyUrl, obj).subscribe((result) => {
        let json: any = this.Service.parserToJSON(result);
        var parsedJSON = JSON.parse(json.string);
        if (parsedJSON[0].Status != "NOTEXIST") {
          this.mobilenoErrMessage = parsedJSON[0].Message;
          this.signupFg.controls['mobileno'].setValue(undefined);
        } else {
          this.mobilenoErrMessage = undefined;
        }

      })
    }
  }

  mailVerification() {
    if (this.signupFg.get('emailid').valid) {
      this.emailidErrMessage = '';
      const obj = {
        emailid: this.signupFg.get('emailid').value
      }
      this.Service.post_data(this.mailVerfyUrl, obj).subscribe((result) => {
        let json: any = this.Service.parserToJSON(result);
        var parsedJSON = JSON.parse(json.string);
        if (parsedJSON[0].Status != "NOTEXIST") {
          this.emailidErrMessage = parsedJSON[0].Message;
          this.signupFg.controls['emailid'].setValue(undefined);
        } else {
          this.emailidErrMessage = undefined;
        }
      })
    }
  }


  validateUsernameUnique = () => {
    if (this.signupFg.get('username').valid) {
      let obj: any = {};
      obj.username = this.signupFg.controls['username'].value;
      this.Service.post_data("getUsrNameExist", obj)
        .subscribe((result) => {
          let json: any = this.Service.parserToJSON(result);
          var parsedJSON = [];
          parsedJSON = JSON.parse(json.string);
          if (parsedJSON[0].Status != "NOTEXIST") {
            this.usernameErrMessage = parsedJSON[0].Message;
            this.signupFg.controls['username'].setValue(undefined);
          } else {
            this.usernameErrMessage = undefined;
          }

        }, (error) => {
          console.log(error);
        });
    }
  }


  MatchPassword = () => {
    var passwordControl = this.signupFg.controls['pwd'].value;
    var confirmPasswordControl = this.signupFg.controls['confirmpassword'].value;
    if (passwordControl !== confirmPasswordControl) {
      this.cpwdErrMessage = "Password and Confirm Password fields should match.";
    } else {
      this.cpwdErrMessage = undefined;
    }
  }

  // =====================VALIDATION END=====================
  // =====================UPLOAD PROFILE IMAGE=====================
  async uploadImage() {
    let actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Take photo',
          role: 'destructive',
          icon: 'camera',
          handler: () => {
            this.captureImage('camera');
          }
        },
        {
          text: 'Choose photo from Gallery',
          icon: 'images',
          handler: () => {
            this.captureImage('gallery');
          }
        },
      ]
    });
    actionSheet.present();
  }

  async captureImage(data: any) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    if (data == 'camera') {
      options.sourceType = this.camera.PictureSourceType.CAMERA;
      this.camera.getPicture(options).then((imageData) => {
        // this.file.resolveLocalFilesystemUrl(imageData).then(fileEntry => {
        //   fileEntry.getMetadata((metadata) => {
        //     let fileSize = metadata.size / 1024 / 1024;
        //     if (fileSize <= 8) {
        this.imageUrlPath = imageData;
        this.currentImage = 'data:image/jpeg;base64,' + imageData;
        this.uploadFile(imageData);

        //     }
        //     else {
        //       alert('file too big');
        //     }
        //   });
        // });
      }, (err) => {
        this.service.create(err, 'top', 'error');
        // Handle error
      });
    } else
      if (data == 'gallery') {
        options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.camera.getPicture(options).then((imageData) => {

          // this.file.resolveLocalFilesystemUrl(imageData).then(fileEntry => {
          //   fileEntry.getMetadata((metadata) => {
          //     let fileSize = metadata.size / 1024 / 1024;
          //     if (fileSize <= 8) {
          this.imageUrlPath = imageData;
          this.currentImage = 'data:image/jpeg;base64,' + imageData;
          this.uploadFile(imageData);

          //     }
          //     else {
          //       alert('file too big');
          //     }
          //   });
          // });
        }, (err) => {
          // Handle error
          this.service.create(err, 'top', 'error');
        });
      }
  }


  async uploadFile(imageData) {
    this.signupFg.controls['base64String'].setValue(imageData);
    this.signupFg.controls['contentType'].setValue("image/jpeg");
    this.signupFg.controls['file_ext'].setValue("jpeg");
  }
  // =========================UPLOAD PROFILE IMAGE END====================================

  // =====================SIGN IN=====================
  async signupFunction() {
    const data = this.signupFg.value;
    data[`gender`] = this.genderList.filter(gender => gender.isSelected).map(g => g.name)[0];
    if (data.gender != null) {
      if (this.signupFg.valid) {
        this.signupFg.removeControl('')
        this.inValidGender = false;
        let obj: any = {};
        obj.mobileno = this.signupFg.controls['mobileno'].value;
        this.Service.post_data_otp("sendOtpViaSMS", obj)
          .subscribe((result) => {
            let json: any = this.Service.parserToJSON(result);
            var parsedJSON = JSON.parse(json.string);
            this.navCtrl.navigateRoot(['/otp', { 'otp': parsedJSON[0]['OTP'], 'mobileno': obj.mobileno, 'data': JSON.stringify(data), 'page': 'signup' }]);
          }, (error) => {
            console.log(error);
          });
      } else if (!this.signupFg.get('base64String').valid || !this.signupFg.get('file_ext').valid || !this.signupFg.get('contentType').valid) {
        this.alertMessage('Upload Image to Proceed Further');
      }
      else {
        this.validateAllFormFields(this.signupFg);
      }
    } else {
      this.inValidGender = true;
    }
  }





  // =====================HIDE PASSWORD=====================
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  cpasswordType: string = 'password';
  cpasswordIcon: string = 'eye-off';

  hideShowcPassword() {
    this.cpasswordType = this.cpasswordType === 'text' ? 'password' : 'text';
    this.cpasswordIcon = this.cpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  // =====================end of HIDE PASSWORD=====================

  async alertMessage(message) {
    const alert = await this.alertController.create({
      cssClass: 'alert-custom-class',
      message: message,
      buttons: [{
        text: 'OK',
        cssClass: 'warning',
      },]
    });
    await alert.present();
  }
}

