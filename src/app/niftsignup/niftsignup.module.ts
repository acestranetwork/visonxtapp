import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NiftsignupPageRoutingModule } from './niftsignup-routing.module';

import { NiftsignupPage } from './niftsignup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    NiftsignupPageRoutingModule
  ],
  declarations: [NiftsignupPage]
})
export class NiftsignupPageModule {}
