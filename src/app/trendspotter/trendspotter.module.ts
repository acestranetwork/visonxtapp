import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TrendspotterPageRoutingModule } from './trendspotter-routing.module';

import { TrendspotterPage } from './trendspotter.page';

import { HeaderPageModule } from '../header/header.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TrendspotterPageRoutingModule, HeaderPageModule
  ],
  declarations: [TrendspotterPage]
})
export class TrendspotterPageModule { }
