import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TrendspotterPage } from './trendspotter.page';

describe('TrendspotterPage', () => {
  let component: TrendspotterPage;
  let fixture: ComponentFixture<TrendspotterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendspotterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TrendspotterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
