import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrendspotterPage } from './trendspotter.page';

const routes: Routes = [
  {
    path: '',
    component: TrendspotterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrendspotterPageRoutingModule {}
