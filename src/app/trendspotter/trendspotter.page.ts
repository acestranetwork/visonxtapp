import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard';
import { GlobalServicesService } from '../global-services.service';
import { IonInfiniteScroll, IonContent } from '@ionic/angular';
@Component({
  selector: 'app-trendspotter',
  templateUrl: './trendspotter.page.html',
  styleUrls: ['./trendspotter.page.scss'],
})
export class TrendspotterPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent) Content: IonContent;
  itemss = [];
  constructor(private menu: MenuController, private service: GlobalServicesService) {

  }
  @ViewChild('slides', { static: true }) slider: IonSlides;
  slideOpt = {
    loop: true
  };
  url = 'getTrendSpotterSearchWithPaging';
  trendSpotterList: any = [];
  trendSpotterListCount = 0;
  city_or_studname = '';
  showToolbar: boolean = true;
  pageNo = 0;
  Scroll = false;
  loadMoreBtn = false;
  infiniteScrollAlter: any;
  ngOnInit() {
    Keyboard.onKeyboardShow().subscribe(() => {
      this.showToolbar = false;
    });
    Keyboard.onKeyboardHide().subscribe(() => {
      this.showToolbar = true;
    });
    this.loadMore();
  }

  ionViewWillEnter() {
    this.service.sendMessage('startNotificationCountFunction');
  }

  isItemAvailable = false;
  items = [];

  isEnableSearch: boolean;

  enableSearch(value) {
    this.isEnableSearch = value;
    if (!value && this.city_or_studname) {
      this.city_or_studname = '';
      this.pageNo = 1;
      this.trendSpotterList = [];
      this.getTrendSpotterList();
    }
    this.Content.scrollToTop();
    // this.loaderRefresh();
  }

  openMenu() {
    this.menu.open();
  }

  selectedItem = "";
  ItemAvailableClose(selected) {
    this.isItemAvailable = false;
    this.selectedItem = selected;
  }
  searchList() {
    this.pageNo = 1;
    this.trendSpotterList = [];
    this.getTrendSpotterList();
  }
  loadMore() {
    this.pageNo = this.pageNo + 1;
    this.loadMoreBtn = false;
    this.getTrendSpotterList();
  }
  getTrendSpotterList() {
    const obj = {
      pageid: this.pageNo,
      rows_in_page: 20,
      city_or_studname: this.city_or_studname
    }
    this.service.post_data(this.url, obj).subscribe((result) => {
      let res: any = this.service.parserToJSON(result);
      this.trendSpotterListCount = JSON.parse(res.string).Total_Records;
      if (this.trendSpotterListCount >= this.trendSpotterList.length) {
        this.trendSpotterList = this.trendSpotterList.concat(JSON.parse(res.string).Details);
        setTimeout(() => {
          if (this.trendSpotterListCount! > this.trendSpotterList.length) {
            this.loadMoreBtn = true;
          } else {
            this.loadMoreBtn = false;
          }
        }, 500);
      }
    })
    // this.loaderRefresh();
  }


  // loadData() {
  //   this.infiniteScrollAlter = this.infiniteScroll;
  //   setTimeout(() => {
  //     this.pageNo = this.pageNo + 1;
  //     this.getTrendSpotterList();
  //     if (this.trendSpotterListCount == this.trendSpotterList.length) {
  //       this.infiniteScroll.disabled = true;
  //     }
  //     this.infiniteScroll.complete();
  //   }, 500);

  // }

  // loaderRefresh() {
  //   if (this.infiniteScrollAlter != undefined) {
  //     this.infiniteScrollAlter.disabled = false;
  //     console.log(this.infiniteScrollAlter.disabled)
  //   }

  // }


}
