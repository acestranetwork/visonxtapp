import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResoursesVideosPage } from './resourses-videos.page';

const routes: Routes = [
  {
    path: '',
    component: ResoursesVideosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResoursesVideosPageRoutingModule {}
