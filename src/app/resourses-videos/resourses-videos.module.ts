import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResoursesVideosPageRoutingModule } from './resourses-videos-routing.module';

import { ResoursesVideosPage } from './resourses-videos.page';

import { HeaderPageModule } from '../header/header.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResoursesVideosPageRoutingModule, HeaderPageModule
  ],
  declarations: [ResoursesVideosPage]
})
export class ResoursesVideosPageModule { }
