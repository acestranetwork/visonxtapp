import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResoursesVideosPage } from './resourses-videos.page';

describe('ResoursesVideosPage', () => {
  let component: ResoursesVideosPage;
  let fixture: ComponentFixture<ResoursesVideosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResoursesVideosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResoursesVideosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
