import { Component, OnInit } from '@angular/core';
import { GlobalServicesService } from '../global-services.service';
import { MenuController, AlertController, NavController, NavParams, PopoverController } from '@ionic/angular';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
@Component({
  selector: 'app-resourses-videos',
  templateUrl: './resourses-videos.page.html',
  styleUrls: ['./resourses-videos.page.scss'],
})
export class ResoursesVideosPage implements OnInit {

  constructor(private streamingMedia: StreamingMedia, public service: GlobalServicesService, private menu: MenuController, public navCtrl: NavController) {

  }
  ngOnInit() {
    this.getResourceList();
  }

  ionViewWillEnter() {
    this.service.sendMessage('startNotificationCountFunction');
  }

  // ====================Variables=========================

  url = 'getResourceList';
  resourceList: any = [];

  // =====================================================



  //video player
  // videoUrl = this.service.STREAMING_VIDEO_URL;
  public play(vdoUrl) {
    let options: StreamingVideoOptions = {
      successCallback: () => { },
      errorCallback: (e) => { console.log('Error streaming') },
      orientation: 'portrait',
      shouldAutoClose: true,
      controls: true
    };
    this.streamingMedia.playVideo(vdoUrl, options);
  }
  openMenu() {
    // this.menu.enable(true, 'first');
    this.menu.open();
  }


  getResourceList() {
    const obj = {};
    this.service.post_data(this.url, obj).subscribe((result) => {
      let res: any = this.service.parserToJSON(result);
      // let json: any = this.service.parserToJSON(result);
      // let jons1 = JSON.stringify(json);
      // let json2 = jons1.replace(/\\n/g, '')
      // var parsedJSON = JSON.parse(JSON.parse(json2).string);
      this.resourceList = JSON.parse(res.string);
    })
  }
}
