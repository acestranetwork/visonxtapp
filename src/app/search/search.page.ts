import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { GlobalServicesService } from '../global-services.service';
import { Keyboard } from '@ionic-native/keyboard';
import { Router, ActivatedRoute, ParamMap, NavigationExtras } from '@angular/router';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  constructor(private menuCtrl: MenuController, public service: GlobalServicesService,
    private router: Router) {

  }

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  // =====================Hide Toolbar when keypad is open=====================
  showToolbar: boolean = true;
  homeSearch: any;
  SeearchedLists: any = [];
  totalRecordCount: any;
  page = 1;
  itemsPerPage = 6;
  loadMoreBtn = false;
  ngOnInit() {
    Keyboard.onKeyboardShow().subscribe(() => {
      this.showToolbar = false;
    });
    Keyboard.onKeyboardHide().subscribe(() => {
      this.showToolbar = true;
    });
  }

  ionViewWillEnter() {
    this.service.sendMessage('startNotificationCountFunction');
  }

  loadData() {
    // setTimeout(() => {
    this.page = this.page + 1;
    this.loadMoreBtn = false;
    this.HomeSearch();
    //   event.target.complete();
    //   if (this.SeearchedLists.length == this.totalRecordCount) {
    //     event.target.disabled = true;
    //   }
    // }, 700);
  }

  // ===================== ON CHANGE SEARCH EVENTS =====================
  HomeSearch() {
    if (this.homeSearch) {
      this.onHomeSearch();
    } else {
      this.SeearchedLists = [];
      this.loadMoreBtn = false;
      this.page = 1;
    }
  }

  onHomeSearch = () => {
    var username = localStorage.getItem('username');
    let obj = {
      'pageid': this.page,
      'rows_in_page': 12,
      'username': username,
      'image_video': 'ALL',
      'image_category': this.homeSearch
    };
    this.service.post_data("getTrendSpotterImagesVideosWithPaging", obj)
      .subscribe((result) => {
        let json: any = this.service.parserToJSON(result);
        let jons1 = JSON.stringify(json);
        let json2 = jons1.replace(/\\n/g, '')
        var parsedJSON = JSON.parse(JSON.parse(json2).string);
        // let search_result = JSON.parse(json.string);
        this.totalRecordCount = parsedJSON.Total_Records;
        if (this.totalRecordCount >= this.SeearchedLists.length) {
          this.SeearchedLists = this.SeearchedLists.concat(parsedJSON.Details);
          setTimeout(() => {
            if (this.totalRecordCount! > this.SeearchedLists.length) {
              this.loadMoreBtn = true;
            } else {
              this.loadMoreBtn = false;
            }
          }, 500);
        }
      }, (error) => {
        console.log(error);
      });
  }

  gotoInnerPage(data) {
    this.router.navigate(['/media-info', { 'data': JSON.stringify(data), 'page': 'search' }]);
  }

  //=====================Open Menu=====================
  openMenu() {
    // this.menu.enable(true, 'first');
    this.menuCtrl.open();
  }

}
