import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadedimagesPage } from './uploadedimages.page';

const routes: Routes = [
  {
    path: '',
    component: UploadedimagesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadedimagesPageRoutingModule {}
