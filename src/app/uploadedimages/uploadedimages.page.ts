import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalServicesService } from '../global-services.service';
import { MenuController, AlertController, NavController, NavParams, PopoverController } from '@ionic/angular';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard';
import { IonInfiniteScroll, IonContent } from '@ionic/angular';
@Component({
  selector: 'app-uploadedimages',
  templateUrl: './uploadedimages.page.html',
  styleUrls: ['./uploadedimages.page.scss'],
})
export class UploadedimagesPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent) Content: IonContent;

  constructor(private menuCtrl: MenuController, public service: GlobalServicesService, public navCtrl: NavController, public popoverController: PopoverController, public alertController: AlertController, private router: Router) {
    this.userName = localStorage.getItem('username');
    // this.userMobileNumber = localStorage.getItem('mobileno');
  }

  // userMobileNumber;
  userName;
  pageNo = 0;
  search = ''
  loadMoreBtn = false;
  // =====================Hide Toolbar when keypad is open=====================
  showToolbar: boolean = true;
  ngOnInit() {
    Keyboard.onKeyboardShow().subscribe(() => {
      this.showToolbar = false;
    });

    Keyboard.onKeyboardHide().subscribe(() => {
      this.showToolbar = true;
    });

    this.loadMore();
  }

  ionViewWillEnter() {
    this.service.sendMessage('startNotificationCountFunction');
  }

  url = 'getTrendSpotterImagesVideosWithPaging';
  uploadImageList: any = [];
  uploadImageListCount = 0;
  //=====================Open Menu=====================
  openMenu() {
    this.menuCtrl.open();
  }

  //=====================Move to Inner Page of Selected Image=====================
  navigateMediaInfo(data) {
    this.router.navigate(['/media-info', { 'data': JSON.stringify(data), 'page': 'image' }]);
  }

  //=====================Search With Auto-Complete=====================
  // Declare the variable (in this case and initialize it with false)
  isItemAvailable = false;
  items = [];


  getItems(ev: any) {

  }
  // Hide/Show serach bar
  isEnableSearch: boolean;
  enableSearch(value) {
    this.isEnableSearch = value;
    if (!value && this.search) {
      // this.infiniteScroll.disabled = false;
      this.search = '';
      this.pageNo = 1;
      this.uploadImageList = [];
      this.getTrendSpottersImageList();
    }
    this.Content.scrollToTop();
  }


  selectedItem = "";
  ItemAvailableClose(selected) {
    this.isItemAvailable = false;
    this.selectedItem = selected;
  }

  searchList() {
    this.pageNo = 1;
    this.uploadImageList = [];
    this.getTrendSpottersImageList();
  }


  getTrendSpottersImageList() {
    const obj = {
      pageid: this.pageNo,
      rows_in_page: 12,
      username: this.userName,
      image_video: 'IMAGE',
      image_category: this.search
    }
    this.service.post_data(this.url, obj).subscribe((result) => {
      // let res: any = this.service.parserToJSON(result);
      let json: any = this.service.parserToJSON(result);
      let jons1 = JSON.stringify(json);
      let json2 = jons1.replace(/\\n/g, '')
      var parsedJSON = JSON.parse(JSON.parse(json2).string);


      this.uploadImageListCount = parsedJSON.Total_Records;

      if (this.uploadImageListCount >= this.uploadImageList.length) {
        this.uploadImageList = this.uploadImageList.concat(parsedJSON.Details);
        setTimeout(() => {
          if (this.uploadImageListCount! > this.uploadImageList.length) {
            this.loadMoreBtn = true;
          } else {
            this.loadMoreBtn = false;
          }
        }, 500);
      }
    })
  }

  // loadData() {
  //   setTimeout(() => {
  //     this.pageNo = this.pageNo + 1;
  //     this.getTrendSpottersImageList();
  //     this.infiniteScroll.complete();
  //     if (this.uploadImageListCount <= this.uploadImageList.length) {
  //       this.infiniteScroll.disabled = true;
  //     }
  //   }, 500);

  // }
  loadMore() {
    this.pageNo = this.pageNo + 1;
    this.loadMoreBtn = false;
    this.getTrendSpottersImageList();
  }
  // toggleInfiniteScroll() {
  //   this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  // }
}
