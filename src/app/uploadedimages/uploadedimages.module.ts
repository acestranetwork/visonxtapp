import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UploadedimagesPageRoutingModule } from './uploadedimages-routing.module';

import { UploadedimagesPage } from './uploadedimages.page';
import { HeaderPageModule } from '../header/header.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UploadedimagesPageRoutingModule,HeaderPageModule
  ],
  declarations: [UploadedimagesPage]
})
export class UploadedimagesPageModule {}
