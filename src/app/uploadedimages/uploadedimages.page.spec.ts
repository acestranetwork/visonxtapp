import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UploadedimagesPage } from './uploadedimages.page';

describe('UploadedimagesPage', () => {
  let component: UploadedimagesPage;
  let fixture: ComponentFixture<UploadedimagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadedimagesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadedimagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
