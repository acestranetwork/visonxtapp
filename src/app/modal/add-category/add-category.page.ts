import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.page.html',
  styleUrls: ['./add-category.page.scss'],
})
export class AddCategoryPage implements OnInit {

  constructor(private popover: PopoverController) { } 

  ngOnInit() {
  }
  ClosePopover() {
    this.popover.dismiss();
  }
}
