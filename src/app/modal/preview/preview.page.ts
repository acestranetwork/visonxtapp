import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
@Component({
  selector: 'app-preview',
  templateUrl: './preview.page.html',
  styleUrls: ['./preview.page.scss'],
})
export class PreviewPage implements OnInit {

  constructor(private popover: PopoverController) { } 

  ngOnInit() {
  }
  ClosePopover() {
    this.popover.dismiss();
  }
}
