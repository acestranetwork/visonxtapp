import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UploadImagesVideosPageRoutingModule } from './upload-images-videos-routing.module';

import { UploadImagesVideosPage } from './upload-images-videos.page';
import { FileUploadModule } from 'ng2-file-upload';
import { HeaderPageModule } from '../header/header.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UploadImagesVideosPageRoutingModule,
    FileUploadModule,HeaderPageModule
  ],
  declarations: [UploadImagesVideosPage]
})
export class UploadImagesVideosPageModule {}
