import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UploadImagesVideosPage } from './upload-images-videos.page';

describe('UploadImagesVideosPage', () => {
  let component: UploadImagesVideosPage;
  let fixture: ComponentFixture<UploadImagesVideosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadImagesVideosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadImagesVideosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
