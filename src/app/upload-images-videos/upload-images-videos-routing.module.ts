import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadImagesVideosPage } from './upload-images-videos.page';

const routes: Routes = [
  {
    path: '',
    component: UploadImagesVideosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadImagesVideosPageRoutingModule {}
