import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { GlobalServicesService } from '../global-services.service';
import { MenuController, AlertController, NavController, NavParams, PopoverController, Platform, ViewWillLeave } from '@ionic/angular';
import { PreviewPage } from '../modal/preview/preview.page';
import { Router, ActivatedRoute, ParamMap, NavigationExtras } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { File } from '@ionic-native/file/ngx';
import { VideoEditor, CreateThumbnailOptions, GetVideoInfoOptions } from '@ionic-native/video-editor';
import { errorMonitor } from 'events';
// declare var CordovaExif: any;
@Component({
  selector: 'app-upload-images-videos',
  templateUrl: './upload-images-videos.page.html',
  styleUrls: ['./upload-images-videos.page.scss'],
  providers: [DatePipe]
})
export class UploadImagesVideosPage implements OnInit, ViewWillLeave {
  @ViewChild('videoElement', { static: false }) myVideo: ElementRef;
  imagePreview;
  type;
  page = "";
  public imageUrlPath;
  fileInfo: any = {};
  fileName: any = '';
  fileSize: any = '';
  imageMaxWidth: number = 720;
  imageMaxheight: number = 1024;
  originalVideo: any;
  originalImage: any;
  isLoading = false;
  newImage: any;
  posterImage = 'assets/images/video_thumbnail.jpg';
  posterImageUrl;
  constructor(public service: GlobalServicesService,
    private menu: MenuController,
    public alertController: AlertController,
    public navCtrl: NavController,
    public popoverController: PopoverController,
    private router: Router,
    private route: ActivatedRoute,
    private camera: Camera,
    private webview: WebView,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public file: File,
    public platform: Platform,
    private datepipe: DatePipe
  ) {

  }

  ngOnInit() {
    //=====================Get params value=====================
    this.route.queryParams.subscribe(
      params => {
        this.type = params['type'];
        this.page = params['page'] ? params['page'] : "";
      }
    )
    this.getUserPosition();
  }

  ionViewWillEnter() {
    this.service.sendMessage('startNotificationCountFunction');
  }
  ionViewWillLeave() {
    if (this.posterImageUrl) {
      this.file.resolveLocalFilesystemUrl(this.posterImageUrl).then((fileEntry) => {
        fileEntry.remove(() => {

        }, (error) => {
          console.log(error)
        });
      }).catch((e) => {
        console.log(e)
      })
    }
  }
  goback() {
    this.type = undefined;
    this.imagePreview = undefined;
    this.imageUrlPath = undefined;
    this.router.navigate(['/home']);
  }

  //capture image with location
  takeSnap() {

    if (this.type == 'image') {
      const cameraoptions: CameraOptions = {
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true,
        quality: 50,
      }
      this.camera.getPicture(cameraoptions).then((imageData) => {
        //   if(CordovaExif){
        //   CordovaExif.readData(imageData, (res) => {
        //     console.log('CordovaExif', res)
        //     this.fileInfo.resolution = res.ImageWidth + 'X' + res.ImageHeight + 'px';
        //     this.fileInfo.device = res.Make;
        //     this.fileInfo.model = res.Model;
        //     this.fileInfo.flash = res.Flash == 'Flash fired, compulsory flash mode' ? 'Flash Fired' : 'No Flash';
        //     this.fileInfo.iso = res.ISOSpeedRatings;
        //     this.fileInfo.whiteBalance = res.WhiteBalance;
        //     this.fileInfo.orientation = res.Orientation == 1 ? 'Portrait' : 'Landscape';
        //     this.fileInfo.latitude = res.GPSLatitude ? res.GPSLatitude : null;
        //     this.fileInfo.longitude = res.GPSLongitude ? res.GPSLongitude : null;
        //     // this.fileInfo.time = res.DateTime;
        //     this.fileInfo.ImageHeight = res.ImageHeight;
        //     this.fileInfo.ImageWidth = res.ImageWidth;
        //   })
        // }
        if (imageData.indexOf("file://") !== -1) {
          this.imageUrlPath = imageData;
          this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(imageData));
        } else if (imageData.indexOf("content://") !== -1) {
          this.imageUrlPath = imageData;
          this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(imageData));
        } else {
          this.imageUrlPath = "file://" + imageData;
          this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc("file://" + imageData));
        }
        if (this.imageUrlPath) {
          this.file.resolveLocalFilesystemUrl(this.imageUrlPath).then(fileEntry => {
            fileEntry.getMetadata((metadata) => {
              let fileSize = metadata.size / 1024 / 1024;
              this.fileName = fileEntry.name;
              this.fileSize = Math.round(fileSize * 100) / 100;
              this.fileInfo.fileName = fileEntry.name;
              this.fileInfo.time = this.datepipe.transform(new Date(metadata.modificationTime), 'yyyy/MM/dd hh:mm:ss a');
              this.fileInfo.size = Math.round(fileSize * 100) / 100 + 'MB';
              localStorage.setItem('fileSize', JSON.stringify(fileSize));
            });
          });
        }
      }, (err) => {
        // Handle error
        console.log(err);
      });
    } else {
      const cameraoptions2: CameraOptions = {
        destinationType: this.camera.DestinationType.FILE_URI, //this.camera.DestinationType.DATA_URL
        mediaType: this.camera.MediaType.VIDEO,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      }
      this.camera.getPicture(cameraoptions2).then((videoData) => {
        let GetVideoInfoOptions: GetVideoInfoOptions = { fileUri: 'file://' + videoData }
        VideoEditor.getVideoInfo(GetVideoInfoOptions).then((data) => {
          this.fileInfo.resolution = data.width + 'x' + data.height + 'px';
          this.fileInfo.duration = data.duration;
          this.fileInfo.bitrate = data.bitrate;
          this.fileInfo.width = data.width;
          this.fileInfo.height = data.height;
          this.fileInfo.orientation = data.orientation;
        }).catch((e) => {
          console.log(e)
          alert(e);
        })
        let splitPath = videoData.split('/');
        let imageNamewithext = splitPath[splitPath.length - 1];
        let imageName = imageNamewithext.split('.')[0];
        let option: CreateThumbnailOptions = { fileUri: videoData, width: this.fileInfo.width, height: this.fileInfo.height, atTime: 0, outputFileName: imageName, quality: 100 };
        // let option: CreateThumbnailOptions = { fileUri: videoData, width: 120, height: 200, atTime: 0, outputFileName: imageName, quality: 100 };
        VideoEditor.createThumbnail(option).then(res => {
          this.posterImageUrl = "file://" + res;
          this.convertFileUritoBase64("file://" + res);
          this.posterImage = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(res));
        }, (error) => {
          console.log(error);
          alert('finally Err==line181' + error);
        })

        if (videoData.indexOf("file://") !== -1 || videoData.indexOf("content://") !== -1) {
          this.imageUrlPath = videoData;
          this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc(videoData));
        } else {
          this.imageUrlPath = "file://" + videoData;
          this.imagePreview = this.service.checkDomSanitizerForIOS(this.webview.convertFileSrc("file://" + videoData));
        }
        if (this.imageUrlPath) {
          this.file.resolveLocalFilesystemUrl(this.imageUrlPath).then(fileEntry => {
            fileEntry.getMetadata((metadata) => {
              let fileMetaSize = metadata.size / 1024 / 1024;
              this.fileName = fileEntry.name;
              this.fileSize = Math.round(fileMetaSize * 100) / 100;
              this.fileInfo.fileName = fileEntry.name;
              this.fileInfo.time = this.datepipe.transform(new Date(metadata.modificationTime), 'yyyy/MM/dd hh:mm:ss a');
              this.fileInfo.size = Math.round(fileMetaSize * 100) / 100 + 'MB';
              localStorage.setItem('videoFileSize', JSON.stringify(fileMetaSize));
            });
          });
        }
      }, (err) => {
        // Handle error
        console.log(err);
      });
    }
   
  }

  convertFileUritoBase64(ImagePath) {
    this.isLoading = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];
    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      // if (localStorage.getItem('videoThumbnailPosterImage')) {
      //   localStorage.removeItem('videoThumbnailPosterImage');
      // }
      localStorage.setItem('videoThumbnailPosterImage', JSON.stringify(base64));

    }, error => {
      console.log("CROP ERROR -> " + JSON.stringify(error));
    });
  }

  deleteImage(path) {
    if (path) {
      this.imageUrlPath = "";
      this.imagePreview = "";
      this.fileName = '';
      this.fileSize = '';
      if (localStorage.getItem('videoFileSize')) {
        localStorage.removeItem('videoFileSize');
      }
      if (localStorage.getItem('fileSize')) {
        localStorage.removeItem('fileSize');
      }
    } else {
      this.presentAlert();
    }
  }

  async moveToPreview(location, tempPath, type) {
    let datauri;
    datauri = (tempPath.indexOf('?') !== -1) ? tempPath.split('?')[0] : tempPath;
    if (localStorage.getItem('originalImg') != undefined) {
      localStorage.removeItem('originalImg');
    }
    if (localStorage.getItem('editedFilters') != undefined) {
      localStorage.removeItem('editedFilters');
    }
    let allowed_size = type == "image" ? 8 : 15;
    if (tempPath) {
      if (type == 'video') {
        if (localStorage.getItem('videoFileSize')) {
          if (JSON.parse(localStorage.getItem('videoFileSize')) <= allowed_size) {
        
            // localStorage.removeItem('videoFileSize');
            this.router.navigate(['/category-selection', { 'fileinfo': JSON.stringify(this.fileInfo), 'location': location, 'latitude': this.latitude, 'longitude': this.longitude, 'mediaPath': datauri, 'type': this.type, 'page': 'upload' }]);
          } else {
            // localStorage.removeItem('videoFileSize');
            this.presentAlert4();
          }
        }
      } else {
        // console.log("width" + this.fileInfo.ImageWidth + "width:" + this.fileInfo.ImageHeight); 
        // if (this.fileInfo.ImageWidth >= this.imageMaxWidth && this.fileInfo.ImageHeight >= this.imageMaxheight) {
        if (localStorage.getItem('fileSize')) {
          if (JSON.parse(localStorage.getItem('fileSize')) <= allowed_size) {
            this.service.changeMessage(datauri);
            this.router.navigate(['/preview-filter', { 'fileinfo': JSON.stringify(this.fileInfo), 'location': location, 'latitude': this.latitude, 'longitude': this.longitude, 'tempPath': datauri, 'type': type, 'page': 'upload' }]);
          } else {
            // localStorage.removeItem('fileSize');
            this.presentAlert3();
          }
        }
        // } else {
        //   this.presentAlert2();
        // }
      }
    } else {
      this.presentAlert();
    }
  }

  async presentPreviewPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PreviewPage,
      cssClass: 'my-custom-class',
      event: ev,
      backdropDismiss: false,
      translucent: true
    });
    return await popover.present();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      message: 'Please Select the File',
      backdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          cssClass: 'warning',
        }
      ]
    });
    await alert.present();
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Note',
      subHeader: 'Image resolution size invalid',
      message: 'please select the image resolution 720px X 1024px',
      backdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          cssClass: 'warning',
        }
      ]
    });
    await alert.present();
  }

  async presentAlert3() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Note',
      subHeader: 'Image size Invalid',
      message: 'file too big, please select less than 8 MB',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentAlert4() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Note',
      subHeader: 'Video size Invalid',
      message: 'file too big, please select less than 15 MB',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentAlert5(type, data) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      subHeader: type,
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }

  openMenu() {
    this.menu.open();
  }

  //=====================Get Current Location=====================
  options;
  currentPos;
  latitude;
  longitude;
  city;
  location;
  getUserPosition() {
    this.options = {
      enableHighAccuracy: false
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      this.currentPos = pos;
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;
      this.getAddress(this.latitude, this.longitude);
    }, (err: PositionError) => {
      console.log("error : " + err.message);
      ;
    })
  }

  // geocoder options
  nativeGeocoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  // get address using coordinates
  getAddress(lat, long) {
    this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
      .then((res: NativeGeocoderResult[]) => {
        var address = this.pretifyAddress(res[0]);
        this.location = this.pretifyAddress(res[0]);
        var value = address.split(",");
        var count = value.length;
        // country = value[count - 1];
        // state = value[count - 2];
        this.city = value[count - 5];
        // alert(address);
        // alert(this.city);
      })
      .catch((error: any) => {
        alert('Error getting location' + error + JSON.stringify(error));
      });
  }

  // address
  pretifyAddress(addressObj) {
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length)
        address += obj[val] + ', ';
    }
    return address.slice(0, -2);
  }
}
