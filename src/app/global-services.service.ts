import { Injectable, EventEmitter } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/internal/Subscription';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams, HttpParameterCodec } from '@angular/common/http';
import { environment } from '../environments/environment.prod';
import { BehaviorSubject, Subject, throwError } from 'rxjs';
import { Platform } from '@ionic/angular';
import { Base64 } from '@ionic-native/base64/ngx';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { File } from '@ionic-native/file/ngx';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';

@Injectable({
  providedIn: 'root'
})
export class GlobalServicesService {
  /******************************Data Service Declarations************************************** */
  public static API_URL = environment.apiUrl;
  public API_IMAGE_URL = environment.image_url;
  headers: any;
  keyval: string = "A2+b2=$";
  public devicePlatform;
  public deviceId;
  //  keyval = `A2+b2=$`;
  /******************************End of Data Service Declarations************************************** */

  private subject = new Subject<any>();
  private subject2 = new Subject<any>();
  emitChangeSource = new Subject<Observable<any>>();
  private messageSource = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();
  constructor(public storage: Storage,
    private http: HttpClient,
    public ngxXml2jsonService: NgxXml2jsonService,
    public platform: Platform,
    public base64: Base64,
    private androidPermissions: AndroidPermissions,
    private file: File,
    public domSanitizer: DomSanitizer,
    private locationAccuracy: LocationAccuracy,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder) {
    // this.headers = new HttpHeaders({
    //   'Content-Type': 'application/x-www-form-urlencoded'
    // });
    // this.getUniqueDeviceID();

    platform.ready().then(() => {
      if (this.platform.is('android')) {
        this.devicePlatform = "android";
      }
      if (this.platform.is('ios')) {
        this.devicePlatform = "ios";
      }
      if (this.platform.is('mobileweb')) {
        this.devicePlatform = "mobileweb";
      }

    });
  }
  sendMessage(message: string) {
    this.subject.next({ text: message });
  }
  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
  clearMessages() {
    this.subject.next();
  }
  changeMessage(message: any) {
    this.messageSource.next(message);
  }
  // getUniqueDeviceID() {
  //   this.uniqueDeviceID.get()
  //     .then((uuid: any) => {
  //       alert('ts page' + uuid);
  //       this.deviceId = uuid;
  //     })
  //     .catch((error: any) => {
  //       this.deviceId = "Error! ${error}";
  //     });
  // }
  STREAMING_VIDEO_URL = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
  public uploadFormData(formData) {
    // return this.http.post<any>(`${this.DJANGO_API_SERVER}/upload/`, formData);
  }

  /******************************Data Service************************************** */
  post_data(particle_url, obj): Observable<any> {

    obj.keyval = this.keyval;
    // var body = JSON.stringify(obj);
    let body = new HttpParams({ encoder: new HttpUrlEncodingCodec() })
    Object.keys(obj).forEach(k => {
      body = body.append(k, obj[k]);
    });

    // .append('keyval', this.keyval);
    // var body = 'keyval=' + this.keyval;
    return this.http.post(GlobalServicesService.API_URL + particle_url, body, { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }, responseType: 'text' });
  }

  post_data_otp(particle_url, obj): Observable<any> {

    obj.keyvalue = this.keyval;
    // var body = JSON.stringify(obj);
    let body = new HttpParams({ encoder: new HttpUrlEncodingCodec() })
    Object.keys(obj).forEach(k => {
      body = body.append(k, obj[k]);
    });
    return this.http.post(GlobalServicesService.API_URL + particle_url, body, { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }, responseType: 'text' });
  }

  get_lists(particle_url): Observable<any> {
    return this.http.get(GlobalServicesService.API_URL + particle_url);
  }

  get_data(particle_url, id): Observable<any> {
    return this.http.get(GlobalServicesService.API_URL + particle_url + '/' + id);
  }

  put_data(particle_url, id, obj): Observable<any> {
    const api_url = GlobalServicesService.API_URL + particle_url;
    const url = `${api_url}/${id}`;
    obj.keyval = this.keyval;
    return this.http.put(url, JSON.stringify(obj), { headers: this.headers });
  }

  delete_data(particle_url, id): Observable<any> {
    const api_url = GlobalServicesService.API_URL + particle_url;
    const url = `${api_url}/${id}`;
    return this.http.delete(url);
  }

  public FileUpload(context: string, fileToUpload): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    const url = environment.apiUrl + context;
    return this.http.post<any>(url, formData);
  }

  /******************************End of Data Service************************************** */

  /******************************xml to json parser************************************** */

  public parserToJSON(context: string) {
    const parser = new DOMParser();
    const xml = parser.parseFromString(context, 'text/xml');
    const obj = this.ngxXml2jsonService.xmlToJson(xml);
    return obj;
  }

  /******************************End of xml to json parser************************************** */
  /******************************ImageData to base64 parser************************************** */
  async imagetobase64(filePath) {
    let result;
    await this.base64.encodeFile(filePath).then((base64File: string) => {
      result = base64File;
    }, (err) => {
      console.log(err);
    });

    return result;
  }
  /******************************End of ImageData to base64 parser************************************** */

  //************************Pass data between pages************************
  private pushData = new Subject<any>();

  publishSomeData(data: any) {
    this.pushData.next(data);
  }

  getObservable(): Subject<any> {
    return this.pushData;
  }
  //************************end of Pass data between pages************************

  //************************ Execute A Function In A Component From Another Component************************
  invokeMenuFunction = new EventEmitter();
  subsVar: Subscription;
  callMenuFunction(loc) {
    this.invokeMenuFunction.emit(loc);
  }
  //************************ End of Execute A Function In A Component From Another Component************************
  // bybass security trust url for ios

  checkDomSanitizerForIOS(data) {
    if (this.platform.is("ios")) {
      return this.domSanitizer.bypassSecurityTrustUrl(data);
    } else {
      return data;
    }
  }


  // Permission Check

  //Location Checking
  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then((result) => {
      if (result.hasPermission) {
        // this.getUserPosition();
        //If having permission show 'Turn On GPS' dialogue
        this.askToTurnOnGPS();
      } else {
        //If not having permission ask for permission
        this.requestGPSPermission();
      }
    },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {

      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              // alert('requestPermission Error requesting location permissions ' + JSON.stringify(error))
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      (res) => {
        // When GPS Turned ON call method to get Accurate location coordinates
        setTimeout(() => {
          this.getUserPosition();
        }, 1000);
      },
      (error) => {
        alert('Error requesting location permissions')
        this.exitApp();
      }
    );
  }

  //========================location=========================
  options;
  currentPos;
  latitude;
  longitude;
  city;
  invoke = new EventEmitter();
  getUserPosition() {
    this.options = {
      enableHighAccuracy: false
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      this.currentPos = pos;
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;
      this.getAddress(this.latitude, this.longitude);
    }, (err: PositionError) => {
      console.log("error : " + err.message);
      ;
    })
  }

  // geocoder options
  nativeGeocoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  // get address using coordinates
  getAddress(lat, long) {
    this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
      .then((res: NativeGeocoderResult[]) => {
        var address = this.pretifyAddress(res[0]);
        var value = address.split(",");
        if (this.platform.is('ios')) {
          this.city = value[value.length - 2];
        }
        else {
          this.city = value[4];
        }
        this.invoke.emit(this.city)
      })
      .catch((error: any) => {
        alert('Error getting location' + error + JSON.stringify(error));
      });
  }

  // address
  pretifyAddress(addressObj) {
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length)
        address += obj[val] + ', ';
    }
    return address.slice(0, -2);
  }

  // =====================APP EXIT=====================
  exitApp() {
    navigator['app'].exitApp();
  }


}

//************************HttpParameterCodec************************
export class HttpUrlEncodingCodec implements HttpParameterCodec {
  encodeKey(k: string): string { return standardEncoding(k); }
  encodeValue(v: string): string { return standardEncoding(v); }
  decodeKey(k: string): string { return decodeURIComponent(k); }
  decodeValue(v: string) { return decodeURIComponent(v); }
}
function standardEncoding(v: string): string {
  return encodeURIComponent(v);
}

export interface fileTransfer {
  name?: string;
  file?: string;
}