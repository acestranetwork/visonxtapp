import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'coverpage',
    pathMatch: 'full'
  },
  {
    path: 'coverpage',
    loadChildren: () => import('./coverpage/coverpage.module').then(m => m.CoverpagePageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'aluminisignup',
    loadChildren: () => import('./aluminisignup/aluminisignup.module').then(m => m.AluminisignupPageModule)
  },
  {
    path: 'niftsignup',
    loadChildren: () => import('./niftsignup/niftsignup.module').then(m => m.NiftsignupPageModule)
  },
  {
    path: 'trendspotter',
    loadChildren: () => import('./trendspotter/trendspotter.module').then(m => m.TrendspotterPageModule)
  },
  {
    path: 'resourse',
    loadChildren: () => import('./resourse/resourse.module').then(m => m.ResoursePageModule)
  },
  {
    path: 'resourses-videos',
    loadChildren: () => import('./resourses-videos/resourses-videos.module').then(m => m.ResoursesVideosPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule)
  },
  {
    path: 'uploadedimages',
    loadChildren: () => import('./uploadedimages/uploadedimages.module').then(m => m.UploadedimagesPageModule)
  },
  {
    path: 'uploadedvideos',
    loadChildren: () => import('./uploadedvideos/uploadedvideos.module').then(m => m.UploadedvideosPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then(m => m.SearchPageModule)
  },
  {
    path: 'upload-images-videos',
    loadChildren: () => import('./upload-images-videos/upload-images-videos.module').then(m => m.UploadImagesVideosPageModule)
  },
  {
    path: 'preview',
    loadChildren: () => import('./modal/preview/preview.module').then(m => m.PreviewPageModule)
  },
  {
    path: 'category-selection',
    loadChildren: () => import('./category-selection/category-selection.module').then(m => m.CategorySelectionPageModule)
  },
  {
    path: 'add-category',
    loadChildren: () => import('./modal/add-category/add-category.module').then(m => m.AddCategoryPageModule)
  },
  {
    path: 'otp',
    loadChildren: () => import('./otp/otp.module').then(m => m.OtpPageModule)
  },
  {
    path: 'terms',
    loadChildren: () => import('./terms/terms.module').then(m => m.TermsPageModule)
  },
  {
    path: 'preview-filter',
    loadChildren: () => import('./preview-filter/preview-filter.module').then(m => m.PreviewFilterPageModule)
  },
  {
    path: 'media-info',
    loadChildren: () => import('./media-info/media-info.module').then(m => m.MediaInfoPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then(m => m.ForgotPasswordPageModule)
  },
  {
    path: 'header',
    loadChildren: () => import('./header/header.module').then(m => m.HeaderPageModule)
  },
  {
    path: 'advertisement-modalpop',
    loadChildren: () => import('./advertisement-modalpop/advertisement-modalpop.module').then(m => m.AdvertisementModalpopPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./notification/notification.module').then(m => m.NotificationPageModule)
  },
  {
    path: 'image-crop-modal',
    loadChildren: () => import('./image-crop-modal/image-crop-modal.module').then(m => m.ImageCropModalPageModule)
  },
  {
    path: 'file-information',
    loadChildren: () => import('./file-information/file-information.module').then(m => m.FileInformationPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
