import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImageCropModalPageRoutingModule } from './image-crop-modal-routing.module';

import { ImageCropModalPage } from './image-crop-modal.page';
import { ImageCropperModule} from 'ngx-image-cropper';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImageCropModalPageRoutingModule,
    ImageCropperModule
  ],
  declarations: [ImageCropModalPage]
})
export class ImageCropModalPageModule {}
