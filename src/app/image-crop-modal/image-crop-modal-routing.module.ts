import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImageCropModalPage } from './image-crop-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ImageCropModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImageCropModalPageRoutingModule {}
