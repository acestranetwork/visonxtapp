import { ChangeDetectionStrategy, Component, OnInit, Input, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { ModalController, Platform} from '@ionic/angular';
import {ImageCroppedEvent, ImageCropperModule,ImageCropperComponent , ImageTransform} from 'ngx-image-cropper';
@Component({
  selector: 'app-image-crop-modal',
  templateUrl: './image-crop-modal.page.html',
  styleUrls: ['./image-crop-modal.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class ImageCropModalPage implements OnInit {
  @ViewChild('imgcropper', {static: false}) imgcropper:ImageCropperComponent
  croppedImageBase64 = '';
  showCropper:boolean = false;
  canvasRotation = 0;
  maintainAspectRatio:boolean = true;
  transform: ImageTransform = {};
  subscription:any;
  constructor(private modalController: ModalController, public platform: Platform) { }
  @Input() imageBase64 = '';
  ngOnInit() {
  }
  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
      this.dismissModal();
    })
  }
  
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  imageLoaded() {
    this.showCropper = true;
}
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImageBase64 = event.base64;
  }

  dismissModal(croppedImageBase64?: string) {
    this.modalController.dismiss({croppedImageBase64});
  }

  rotateLeft() {
    this.canvasRotation--;
    this.flipAfterRotate();
}

rotateRight() {
    this.canvasRotation++;
    this.flipAfterRotate();
}


toggleMaintainAspectRatio() {
  setTimeout(() => {
    this.imgcropper.resetCropperPosition();  
  }, 100);

  this.maintainAspectRatio = !this.maintainAspectRatio;

}

private flipAfterRotate() {
  const flippedH = this.transform.flipH;
  const flippedV = this.transform.flipV;
  this.transform = {
      ...this.transform,
      flipH: flippedV,
      flipV: flippedH
  };
}

flipHorizontal() {
  this.transform = {
      ...this.transform,
      flipH: !this.transform.flipH
  };
}

flipVertical() {
  this.transform = {
      ...this.transform,
      flipV: !this.transform.flipV
  };
}
}
