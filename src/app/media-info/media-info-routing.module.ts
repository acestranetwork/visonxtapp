import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MediaInfoPage } from './media-info.page';

const routes: Routes = [
  {
    path: '',
    component: MediaInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MediaInfoPageRoutingModule {}
