import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MediaInfoPage } from './media-info.page';

describe('MediaInfoPage', () => {
  let component: MediaInfoPage;
  let fixture: ComponentFixture<MediaInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MediaInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
