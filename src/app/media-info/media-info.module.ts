import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MediaInfoPageRoutingModule } from './media-info-routing.module';

import { MediaInfoPage } from './media-info.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MediaInfoPageRoutingModule
  ],
  declarations: [MediaInfoPage]
})
export class MediaInfoPageModule {}
