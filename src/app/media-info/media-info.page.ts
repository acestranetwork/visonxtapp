import { Component, OnInit } from '@angular/core';
// import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { IonToggle, ModalController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { GlobalServicesService } from '../global-services.service';
import { ColorPickerComponent } from 'ngx-color-picker';
import { FileInformationPage } from '../file-information/file-information.page';

@Component({
  selector: 'app-media-info',
  templateUrl: './media-info.page.html',
  styleUrls: ['./media-info.page.scss'],
})
export class MediaInfoPage implements OnInit {

  constructor(public service: GlobalServicesService, 
    // private socialSharing: SocialSharing,
     public modalController: ModalController, 
     private route: ActivatedRoute,
    public alertController: AlertController) { }
  page;
  backUrl;
  data: any = {};
  fileinfo: any = {};
  level4Val_Value: any;
  latitude = '';
  longitude = '';
  categories: any = [];
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.page = params['page'];
      this.data = JSON.parse(params['data']);
    });
    //===============Category==============
    this.latitude = String(this.data.lati_longi).split(',')[0];
    this.longitude = String(this.data.lati_longi).split(',')[1]
    this.fileinfo = JSON.parse(this.data.fileinfo);
    if (this.data.level4Val != "") {
      if (JSON.parse(this.data.level4Val).length > 0) {
        this.level4Val_Value = JSON.parse(this.data.level4Val);
      } else {
        this.level4Val_Value = [];
      }
    } else {
      this.level4Val_Value = [];
    }
    this.categories = this.data.img_categ.split(',');
    //===============backURL===================
    if (this.page == 'image') {
      this.backUrl = 'uploadedimages';
    } else if (this.page == 'video') {
      this.backUrl = 'uploadedvideos';
    } else {
      this.backUrl = 'home';
    }
  }




  // =====================Social Media Sharing=====================

  //Twitter
  twitterShare() {
    // this.socialSharing.shareViaTwitter("Shared Post", this.data.upload_image, null)
    //   .then(() => {
    //   }).catch((err) => {
    //     alert("Twitter not found on your phone");
    //   });
  }

  //Instagram
  instagramShare() {
    // this.socialSharing.shareViaInstagram("Shared Post", this.data.upload_image)
    //   .then(() => {
    //   }).catch((err) => {
    //     alert("An error occurred " + err);
    //   });
  }

  //Facebook
  facebookShare() {
    // this.socialSharing.shareViaFacebookWithPasteMessageHint("Shared Post", this.data.upload_image, null, 'Copia Pega!')
    //   .then(() => {
    //   }).catch((err) => {
    //     alert("An error occurred " + err);
    //   });
  }

  // =====================Image Viewer=====================

  async openViewer(image, loc) {
    const modal = await this.modalController.create({
      component: ViewerModalComponent,
      componentProps: {
        src: image, // required
        title: 'Photo Viewer', // optional
        text: 'Location -' + loc// optional
      },
      cssClass: 'ion-img-viewer', // required
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();
  }

  // =====================File Information ALert=====================

  async FileInformationAlert() {

    const modal = await this.modalController.create({
      component: FileInformationPage,
      componentProps: {
        title: 'File Information',
        fileinfo: this.fileinfo,
        data: this.data,
        latitude: this.latitude,
        longitude: this.longitude,
      },
      cssClass: 'my-custom-class', // required
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();

    // const alert = await this.alertController.create({
    //   cssClass: 'alert-custom-class',
    //   header: 'File Information',
    //   message: '<ion-grid *ngIf="data.image_video && data.image_video=="IMAGE"> <ion-row *ngIf="fileinfo.fileName && fileinfo.fileName!=undefined"> <ion-col size="5"> <small><b>File Name :</b></small> </ion-col> <ion-col size="7"> <small>'+this.fileinfo.fileName+'</small>  </ion-col> </ion-row> <ion-row *ngIf="fileinfo.time && fileinfo.time!=undefined">   <ion-col size="5">   <small><b>Time :</b></small> </ion-col>  <ion-col size="7"> <small>{{fileinfo.time}}</small> </ion-col> </ion-row><ion-row *ngIf="fileinfo.resolution && fileinfo.resolution!=undefined"> <ion-col size="5"> <small><b>Resolution :</b></small>  </ion-col>  <ion-col size="7">   <small>{{fileinfo.resolution}}</small> </ion-col> </ion-row> <ion-row *ngIf="fileinfo.size && fileinfo.size!=undefined">   <ion-col size="5"> <small><b>Size :</b></small>  </ion-col> <ion-col size="7"> <small>{{fileinfo.size}}</small> </ion-col> </ion-row> <ion-row *ngIf="fileinfo.device && fileinfo.device!=undefined">  <ion-col size="5">   <small><b>Device :</b></small> </ion-col> <ion-col size="7">   <small>{{fileinfo.device}}</small>  </ion-col>  </ion-row> <ion-row *ngIf="fileinfo.model && fileinfo.model!=undefined">  <ion-col size="5"> <small><b>Model :</b></small> </ion-col> <ion-col size="7"> <small>{{fileinfo.model}}</small>  </ion-col> </ion-row> <ion-row *ngIf="fileinfo.flash && fileinfo.flash!=undefined"> <ion-col size="5"> <small><b>Flash :</b></small> </ion-col> <ion-col size="7"><small>{{fileinfo.flash}}</small>  </ion-col> </ion-row>  <ion-row *ngIf="fileinfo.whiteBalance && fileinfo.whiteBalance!=undefined"> <ion-col size="5"> <small><b>White Blance :</b></small>  </ion-col> <ion-col size="7"> <small>{{fileinfo.whiteBalance}}</small>  </ion-col>  </ion-row> <ion-row *ngIf="fileinfo.iso && fileinfo.iso!=undefined">  <ion-col size="5"> <small><b>ISO :</b></small> </ion-col>  <ion-col size="7"> <small>{{fileinfo.iso}}</small>   </ion-col>  </ion-row><ion-row *ngIf="fileinfo.orientation && fileinfo.orientation!=undefined"> <ion-col size="5">  <small><b>Orientation :</b></small> </ion-col> <ion-col size="7">  <small>{{fileinfo.orientation}}</small> </ion-col> </ion-row>  <ion-row *ngIf="data.location && data.location!=undefined"> <ion-col size="5">  <small><b>Location :</b></small>  </ion-col> <ion-col size="7">  <small>{{data.location?data.location:"No location found}}</small>  </ion-col>  </ion-row> <ion-row *ngIf="latitude"> <ion-col size="5"> <small><b>Latitude :</b></small>  </ion-col>  <ion-col size="7">  <small>{{latitude}}</small> </ion-col> </ion-row> <ion-row *ngIf="longitude"> <ion-col size="5">   <small><b>Longitude :</b></small>  </ion-col> <ion-col size="7"> <small>{{longitude}}</small>  </ion-col> </ion-row> </ion-grid>' ,
    //   // message: '<a class="close">&times;</a> <br><br> <img src="../../assets/image/logo.png" alt="g-maps" style="border-radius: 2px">',
    //   backdropDismiss: false,
    //   buttons: [
    //     {
    //       text: 'Close',
    //       role: 'cancel',
    //       cssClass: 'secondary',
    //       handler: (blah) => {

    //       }
    //     }
    //   ]
    // });
    // await alert.present();
  }

}
