import { Component, OnInit } from '@angular/core';
import { GlobalServicesService } from '../global-services.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  constructor(private service: GlobalServicesService) {
    this.userName = localStorage.getItem('username');
  }

  ngOnInit() {
    this.loadMore();
  }
  notificationUrl = 'getNotificationText';
  notificationStatusUrl = 'setNotificationTextAsRead';
  notificationList: any = [];
  userName;
  totalListCount;
  shownGroup: any = null;
  page = 0;
  loadMoreBtn = false;
  getnotificationsList() {
    const obj = {
      msg_id: 0,
      username: this.userName,
      rows_in_page: 10,
      pageid: this.page
    }
    this.service.post_data(this.notificationUrl, obj).subscribe((result) => {
      let json: any = this.service.parserToJSON(result);
      let list = JSON.parse(json.string);
      if (list) {
        this.totalListCount = list.Total_Records;
        if (this.totalListCount >= this.notificationList.length) {
          this.notificationList = this.notificationList.concat(list.Details);
          setTimeout(() => {
            if (this.totalListCount! > this.notificationList.length) {
              this.loadMoreBtn = true;
            } else {
              this.loadMoreBtn = false;
            }
          }, 500);
        }
      }
    })
  }

  ionViewDidLoad() {

  }



  get_read_notification(id, status, group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
    if (status == 'False') {
      const obj = {
        msg_id: id,
        username: this.userName
      }
      this.service.post_data(this.notificationStatusUrl, obj).subscribe((res) => {
        let json: any = this.service.parserToJSON(res);
        let response = JSON.parse(json.string)[0];
        if (response.Status == 'SUCCESS') {
          this.notificationList.forEach((element, i) => {
            if (i == group) {
              element.read_sts = 'True';
            }
          });
        }
      }, (error) => {
        console.log(error)
      })
    }

  }
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  loadMore() {
    this.page = this.page + 1;
    this.loadMoreBtn = false;
    this.getnotificationsList();
  }

}
