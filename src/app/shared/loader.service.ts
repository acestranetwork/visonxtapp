import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LoadingController } from '@ionic/angular';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor(public loadingController: LoadingController, public NavController: NavController) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const loading = this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      spinner:'circular'
    });

    loading.then(v => v.present());
    return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        loading.then(v => v.dismiss());
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          alert('Error 401');
          this.NavController.navigateRoot(['/login']);
        } else if (err.status === 429) {
          alert('Error');
          this.NavController.navigateRoot(['/login']);
        } else if (err.status == 0) {
          alert('Internet Error, Please Check Your Internet Connection');
        }
        loading.then(v => v.dismiss());
      }
    }));
    return next.handle(req);
  }

}


