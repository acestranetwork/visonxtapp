import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AlertController, NavController, NavParams } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalServicesService } from '../global-services.service';
@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TermsPage implements OnInit {
  data: any = {};
  constructor(public alertController: AlertController, private route: ActivatedRoute, public navCtrl: NavController, private router: Router, public service: GlobalServicesService) {
    this.route.params.subscribe((params) => {
      this.data = params['data'];
    });
  }
  ngOnInit() {
  }
  async acceptAlert(message) {
    const alert = await this.alertController.create({
      cssClass: 'alert-custom-class',
      message: message,
      buttons: [
        {
          text: 'OK',
          cssClass: 'warning',
          handler: () => {
            this.navCtrl.navigateForward(['/login']);
          }
        },
      ]
    });
    await alert.present();
  }

  // =====================SIGN IN=====================
  Message;
  signupFunction() {
    this.service.post_data("studRegisBase64", JSON.parse(this.data))
      .subscribe((result) => {
        let json: any = this.service.parserToJSON(result);
        var parsedJSON = JSON.parse(json.string);
        this.Message = parsedJSON[0].Message;
        this.acceptAlert(this.Message);
      }, (error) => {
        console.log(error);
      });
  }

  declineAlert() {
    this.navCtrl.navigateForward(['aluminisignup']);
  }

}
