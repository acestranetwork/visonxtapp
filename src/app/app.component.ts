import { Component, OnInit } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MenuController, NavController, NavParams, Platform, AlertController } from '@ionic/angular'; //import MenuController to access toggle() method.
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { GlobalServicesService } from './global-services.service';
import { ToastController } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';

import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  devicePlatform;
  username;
  loggedDetails: any = {};
  public alertShown: boolean = false;
  public counter = 0;

  ShowSplash = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public menuCtrl: MenuController,
    public navCtrl: NavController, private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation, private nativeGeocoder: NativeGeocoder, public service: GlobalServicesService,
    public toastController: ToastController, private device: Device,
    public alertController: AlertController, private _location: Location,
    private router: Router,
  ) {

    this.initializeApp();
    this.service.getObservable().subscribe((data) => {
      this.username = data.user;


    });
    this.service.invoke.subscribe((res) => {
      if (res) {
        this.cityChange(res);
      }
    })
  }
  //============get device ID ==================
  deviceId;
  city;
  getUniqueDeviceID() {
    localStorage.setItem('deviceId', this.device.uuid);
    localStorage.setItem('platform', this.device.platform);
  }

  async initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.getUniqueDeviceID();
      this.requestpermission();
      if (this.service.subsVar == undefined) {
        this.service.subsVar = this.service.invokeMenuFunction.subscribe((city: string) => {
          // if (!city) {
          // this.city = city;
          // }
          this.userInfo();
          // localStorage.setItem('location', this.city);
        });
      }

      this.platform.backButton.subscribe(() => {
        if (this.router.url == '/coverpage') {
          if (this.counter == 0) {
            this.counter++;
            this.presentUrExistToast();
            setTimeout(() => { this.counter = 0 }, 2000)
          } else {
            navigator['app'].exitApp();
          }
        } else if (this.router.url == '/home') {
          if (this.counter == 0) {
            this.counter++;
            this.presentUrExistToast();
            setTimeout(() => { this.counter = 0 }, 2000)
          } else {
            navigator['app'].exitApp();
          }
        }
      })
      if (localStorage.getItem('login_token') == '5$/7@nift)8G63') {
        this.username = localStorage.getItem('username');
        this.userInfo();
        this.navCtrl.navigateRoot(['/home']);
      }
    });
  }

  async requestpermission() {
    this.androidPermissions.requestPermissions(
      [
        this.androidPermissions.PERMISSION.CAMERA,
        this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
        this.androidPermissions.PERMISSION.READ_MEDIA_VIDEO,
        this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE,
        this.androidPermissions.PERMISSION.READ_PHONE_STATE,
        this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION,
        this.androidPermissions.PERMISSION.RECEIVE_SMS,
        this.androidPermissions.PERMISSION.READ_SMS
      ]
    ).finally(() => {
      setTimeout(() => {
        this.service.checkGPSPermission();
      }, 2000);
    });
  }

  async presentUrExistToast() {
    const toast = await this.toastController.create({
      message: 'Press again to close VisionXT',
      cssClass: 'ToastMsgCenter',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  async presentConfirm() {
    const alert = await this.alertController.create({
      message: 'Do you want Exit?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.alertShown = false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            navigator['app'].exitApp();
            // this.platform.exitApp();
          }
        }
      ]
    });
    await alert.present().then(() => {
      this.alertShown = false;
    });
  }
  toggleMenu(page) {
    if (page) {
      this.navCtrl.navigateRoot(page);
    }
    this.menuCtrl.toggle();
  }
  // =====================Logout=====================
  logout = () => {
    if (this.username) {
      // alert('click');
      let obj = {
        activity_log: "logout",
        user_name: this.username,
      }
      this.service.post_data("updateActivityLog", obj)
        .subscribe((result) => {
          this.navCtrl.navigateRoot(['/coverpage']);
          this.menuCtrl.toggle();
          localStorage.setItem('login_token', 'N7logout&+2G');
          localStorage.setItem('username', '');
          localStorage.setItem('mobileno', '');
          localStorage.setItem('stud_type', '');
          localStorage.setItem('upload_image', '');
        }, (error) => {
          console.log(error);
        });
    }
    this.loggedDetails = {};
  }
  //=====================get Login Details=====================

  age;
  // username= localStorage.getItem('username');
  userInfo() {
    if (this.username) {
      let obj = {
        username: this.username,
      }
      this.service.post_data("getAlumniOthersInfo", obj)
        .subscribe((result) => {
          let json: any = this.service.parserToJSON(result);
          var parsedJSON = JSON.parse(json.string);
          this.loggedDetails = parsedJSON[0];
        }, (error) => {
          console.log(error);
        });
    }
  }

  cityChange(city) {
    if (!this.city) {
      this.city = city;
      localStorage.setItem('location', this.city);
    }
  }
  // =====================End of Degree list=====================

}
