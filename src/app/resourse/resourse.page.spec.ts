import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResoursePage } from './resourse.page';

describe('ResoursePage', () => {
  let component: ResoursePage;
  let fixture: ComponentFixture<ResoursePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResoursePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResoursePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
