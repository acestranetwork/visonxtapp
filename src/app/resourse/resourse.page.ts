import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { GlobalServicesService } from '../global-services.service';
@Component({
  selector: 'app-resourse',
  templateUrl: './resourse.page.html',
  styleUrls: ['./resourse.page.scss'],
})
export class ResoursePage implements OnInit {

  constructor(private menu: MenuController, private service: GlobalServicesService) { }

  ngOnInit() {
   
  }









  openMenu() {
    // this.menu.enable(true, 'first');
    this.menu.open();
  }



}
///////////
// import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
// import { GlobalServicesService } from '../global-services.service';
// import { ActionSheetController, MenuController, AlertController, NavController, PopoverController } from '@ionic/angular';
// import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { Crop, CropOptions } from '@ionic-native/crop/ngx';
// import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
// import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
// import { File, FileEntry } from '@ionic-native/File/ngx';
// import { Platform } from '@ionic/angular';
// import { WebView } from '@ionic-native/ionic-webview/ngx';

// import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
// import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';

// @Component({
//   selector: 'app-preview-filter',
//   templateUrl: './preview-filter.page.html',
//   styleUrls: ['./preview-filter.page.scss'],
// })

// export class PreviewFilterPage implements OnInit {
//   @ViewChild('videoElement', { static: false }) myVideo: ElementRef;
//   @ViewChild('imageSrc') imageSrc: ElementRef;
//   location: any;
//   tempPath: string;
//   page: string;
//   originalImage: any = "../../assets/image/4.jpg";
//   enableEditMode: boolean = false;
//   croppedImagepath = "";
//   isLoading = false;
//   constructor(private file: File, private crop: Crop, public actionSheetController: ActionSheetController, public alertController: AlertController, public service: GlobalServicesService, private menu: MenuController, public navCtrl: NavController, private route: ActivatedRoute,
//     private router: Router, private androidPermissions: AndroidPermissions, private webview: WebView, private geolocation: Geolocation, private nativeGeocoder: NativeGeocoder) {
//     this.getUserPosition();
//   }

//   ngOnInit() {
//     // =====================Check app permissions===================== 
//     this.checkPermissions();
//     // =====================Get params value===================== 
//     this.route.params.subscribe((params) => {
//       this.location = params['location'];
//       this.type = params['type'];
//       this.tempPath = params['tempPath'];
//       this.page = params['page'];
//       this.originalImage = this.tempPath;
//     });
//     if (this.type == 'video') {
//       alert('VideoPlaybackQuality' + this.tempPath);
//       this.openFile(this.tempPath);
//     } else {
//       this.originalImage = this.webview.convertFileSrc(this.originalImage); // convert to weburl
//     }

//     // if (this.originalImage == undefined) {
//     //   this.service.get('imageUrlPath').then(result => {
//     //     if (result != null) {
//     //       this.tempPath = result;
//     //       if (this.originalImage && this.type == 'video') {
//     //         this.openFile(result);
//     //       } else {
//     //         this.originalImage = this.webview.convertFileSrc(result);
//     //       }
//     //     }
//     //   }).catch(e => {
//     //     console.log('error: ' + e);
//     //   });
//     // }
//   }

//   ngAfterViewInit() {
//     if (this.type == 'video') {
//       let video = this.myVideo.nativeElement;
//       video.src = this.webview.convertFileSrc(this.originalImage);
//       // video.src = "../../assets/videos/sample.mp4";
//       video.play();
//     } else {
//       this.originalImage = this.webview.convertFileSrc(this.originalImage);
//     }
//     // =====================Get params value=====================  
//     // if (this.originalImage == undefined) {
//     //   this.service.get('imageUrlPath').then(result => {
//     //     if (result != null) {
//     //       this.tempPath = result;
//     //       if (this.originalImage && this.type == 'video') {
//     //         this.openFile(result);
//     //       } else {
//     //         this.originalImage = this.webview.convertFileSrc(result);
//     //       }
//     //     }
//     //   }).catch(e => {
//     //     console.log('error: ' + e);
//     //   });
//     // } else {
//     //   if (this.originalImage && this.type == 'video') {
//     //     this.openFile(this.originalImage);
//     //   } else {
//     //     this.originalImage = this.webview.convertFileSrc(this.originalImage); // convert to weburl
//     //   }

//     // }

//   }

//   // =====================Open the video file===================== 
//   openFile(file) {
//     console.log(this.myVideo, document.getElementById('videoElement'));
//     let video = this.myVideo.nativeElement;
//     video.src = this.webview.convertFileSrc(file);
//     video.play();
//   }

//   // =====================Check app permissions===================== 
//   checkPermissions() {
//     this.androidPermissions.requestPermissions(
//       [
//         this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
//         this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
//       ]
//     );
//   }

//   // =====================Edit the Image===================== 
//   // When Sliders Values Change, Update the CSS Variables
//   property;
//   type;
//   filter: string = '';
//   transform: string = '';
//   rotate = "";
//   rotateX = "";
//   rotateY = "";
//   brightness = "";
//   contrast = "";
//   saturate = "";
//   hue_rotate = "";
//   cntProperty: string = "";
//   cntValue: string = "0";
//   cntType: string = "";
//   cropOptions: CropOptions = {
//     quality: 50, targetHeight: 1280, targetWidth: 720
//   }

//   // Add filters to the image
//   setFilter(property, value, unit, type) {
//     // console.log(property, value,unit);
//     this.cntProperty = property;
//     this.cntValue = value;
//     this.cntType = type;
//     console.log(this.cntValue);
//     var img = document.getElementById("image") as HTMLImageElement;
//     if (property == 'rotateY') {
//       this.rotateY = (property + "(" + value + unit + ")");
//     }
//     if (property == 'rotateX') {
//       this.rotateX = (property + "(" + value + unit + ")");
//     }
//     if (property == 'rotate') {
//       this.rotate = (property + "(" + value + unit + ")");
//       // this.DrawImage(this.cntType, this.cntProperty, this.cntValue);
//     }
//     if (property == 'brightness') {
//       this.brightness = (property + "(" + value + unit + ")");
//       console.log(this.brightness);
//     }
//     if (property == 'contrast') {
//       this.contrast = (property + "(" + value + unit + ")");
//     }
//     if (property == 'saturate') {
//       this.saturate = (property + "(" + value + unit + ")");
//     }
//     if (property == 'hue-rotate') {
//       this.hue_rotate = (property + "(" + value + unit + ")");
//     }

//     console.log(this.filter, this.type);
//     if (type == 'transform') {
//       this.transform = this.rotateY + ' ' + this.rotateX + ' ' + this.rotate;
//       console.log(this.transform);
//       // img.style.transform = this.transform;  //(property + "(" + value + unit + ")");
//       this.DrawImage(this.cntType, this.cntProperty, this.cntValue);
//     }
//     if (type == 'filter') {
//       // console.log(this.filter, this.transform);
//       this.filter = this.hue_rotate + ' ' + this.saturate + ' ' + this.brightness + ' ' + this.contrast;
//       img.style.filter = this.filter;
//       // context.filter = this.filter;
//     }
//   }

//   // Save the changes and convert to the image
//   saveChanges() {
//     this.property = null;
//     if (this.cntType == 'filter')
//       this.DrawImage(this.cntType, this.cntProperty, this.cntValue);
//   }
//   // Convert to the image
//   canvasImage;
//   DrawImage(type, property, value) {
//     console.log(type, property, value)
//     var img = document.getElementById("image") as HTMLImageElement;
//     var data, ofilter = this.filter;
//     var canvas = document.createElement('canvas') as HTMLCanvasElement;
//     var context = canvas.getContext('2d');

//     if (type == 'filter') {
//       canvas.width = img.offsetWidth;
//       canvas.height = img.offsetHeight;
//       context.filter = ofilter;
//       context.drawImage(img, 0, 0, canvas.width, canvas.height);
//       data = canvas.toDataURL("image/jpg", 1.0);
//     }
//     if (type == 'transform') {
//       canvas.width = img.offsetWidth;
//       canvas.height = img.offsetHeight;
//       context.filter = ofilter;

//       if (property == 'rotateY' && value == '180') {
//         context.translate(canvas.width, 0);
//         context.scale(-1, 1);
//         context.drawImage(img, 0, 0, canvas.width, canvas.height);
//         data = canvas.toDataURL("image/jpg", 1.0);
//         img.src = data;
//       }
//       if (property == 'rotateY' && value == '360') {
//         img.src = this.originalImage;
//       }

//       if (property == 'rotate' && value == '-90') {
//         var image = document.createElement("img");
//         var ratio;
//         image.onload = function () {
//           canvas.width = 100;
//           canvas.height = 150;
//           ratio = canvas.height / canvas.width;
//           console.log(ratio);
//           context.drawImage(img, 0, 0, canvas.width, canvas.height);
//         }
//         context.clearRect(0, 0, canvas.width, canvas.height);
//         context.save();
//         context.translate(canvas.width / 2, canvas.height / 2);
//         context.rotate(270 * Math.PI / 180);
//         if ((270 - 90) % 180 == 0)
//           context.scale(ratio, ratio);
//         context.translate(-canvas.width / 2, -canvas.height / 2);
//         context.drawImage(img, 0, 0, canvas.width, canvas.height);
//         data = canvas.toDataURL("image/jpg", 1.0);
//         img.src = data;
//         // this.originalImage = data;
//       }
//       if (property == 'rotate' && value == '0') {
//         // context.drawImage(img, 0, 0, canvas.width, canvas.height);
//         img.src = this.originalImage;
//       }
//     }
//     // this.originalImage = data; 
//     this.canvasImage = data;
//   }


//   //  Select the filter property
//   selectedFilterProperty(value) {
//     this.property = value;
//   }
//   // =====================Image Crop===================== 
//   // Crop the image
//   cropImage(imgPath) {
//     let imgPathUrl = imgPath;
//     // let imgPathUrl = (this.canvasImage); 
//     this.crop.crop(imgPathUrl, this.cropOptions)
//       .then(
//         newPath => {
//           // alert(newPath);
//           // alert(newPath.split('?')[0]);
//           this.showCroppedImage(newPath.split('?')[0])
//         },
//         error => {
//           console.log("CROP ERROR -> " + JSON.stringify(error));
//           alert("CROP ERROR-cropImage: " + JSON.stringify(error));
//         }
//       );
//   }

//   // Show and save the cropped image
//   showCroppedImage(ImagePath) {
//     this.isLoading = true;
//     var copyPath = ImagePath;
//     var splitPath = copyPath.split('/');
//     var imageName = splitPath[splitPath.length - 1];
//     var filePath = ImagePath.split(imageName)[0];
//     this.tempPath = ImagePath;
//     // alert(ImagePath); 
//     this.file.readAsDataURL(filePath, imageName).then(base64 => {
//       this.originalImage = base64;
//     }, error => {
//       console.log("CROP ERROR -> " + JSON.stringify(error));
//       alert("CROP ERROR - showCroppedImage: " + JSON.stringify(error));
//     });
//   }


//   // =====================Enable Image Edit ===================== 
//   enableEdit() {
//     this.enableEditMode = true;
//   }

//   // =====================Forward the video data to next page =====================  
//   submitFileData() {
//     // alert('submitFileData --- ' + this.location);
//     this.service.set('imageUrlPath', this.tempPath).then(result => {
//       console.log('Data is saved');
//       // alert('perview img'+result);
//     }).catch(e => {
//       console.log("error: " + e);
//     });
//     // console.log(this.canvasImage ? "this.canvasImage" : "this.originalImage");
//     // alert('category-selection' + this.canvasImage ? this.canvasImage : this.originalImage);
//     alert(this.tempPath);
//     var media;
//     if (this.originalImage.indexOf("http:") !== -1) {
//       media = this.canvasImage ? this.canvasImage : this.tempPath;
//     } else {
//       media = this.tempPath;
//     }
//     if (this.type == 'video') {
//       this.router.navigate(['/category-selection', { 'location': this.location, 'mediaPath': this.tempPath, 'type': this.type, 'page': this.page }]);
//     } else {
//       this.router.navigate(['/category-selection', { 'location': this.location, 'mediaPath': media, 'type': this.type, 'page': this.page }]);
//     }

//   }

//   //=====================Open Menu=====================
//   openMenu() {
//     // this.menu.enable(true, 'first');
//     this.menu.open();
//   }

//   //=====================Goto Back=====================
//   goBack() {
//     if (this.page == 'home') {
//       this.router.navigate(['/home', { 'type': this.type, 'page': this.page }]);
//     } else {
//       this.router.navigate(['/upload-images-videos', { 'type': this.type, 'page': this.page }]);
//     }
//   }


//   //=====================Get Current Location=====================
//   options: GeolocationOptions;
//   currentPos: Geoposition;
//   locationTraces = [];
//   latitude: any = 0; //latitude
//   longitude: any = 0; //longitude
//   address: string;
//   getUserPosition() {
//     this.options = {
//       enableHighAccuracy: false
//     };
//     this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
//       this.currentPos = pos;
//       this.latitude = pos.coords.latitude;
//       this.longitude = pos.coords.longitude;

//       this.getAddress(this.latitude, this.longitude);
//       console.log(pos);
//     }, (err: PositionError) => {
//       console.log("error : " + err.message);
//       ;
//     })
//   }

//   // geocoder options
//   nativeGeocoderOptions: NativeGeocoderOptions = {
//     useLocale: true,
//     maxResults: 5
//   };

//   // get address using coordinates
//   getAddress(lat, long) {
//     this.nativeGeocoder.reverseGeocode(lat, long, this.nativeGeocoderOptions)
//       .then((res: NativeGeocoderResult[]) => {
//         this.address = this.pretifyAddress(res[0]);
//         this.location = this.pretifyAddress(res[0]);

//       })
//       .catch((error: any) => {
//         alert('Error getting location' + error + JSON.stringify(error));
//       });
//   }

//   // address
//   pretifyAddress(addressObj) {
//     let obj = [];
//     let address = "";
//     for (let key in addressObj) {
//       obj.push(addressObj[key]);
//     }
//     obj.reverse();
//     for (let val in obj) {
//       if (obj[val].length)
//         address += obj[val] + ', ';
//     }

//     return address.slice(0, -2);
//   }

// }
