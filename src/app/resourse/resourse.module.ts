import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ResoursePageRoutingModule } from './resourse-routing.module';
import { ResoursePage } from './resourse.page';
import { HeaderPageModule } from '../header/header.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResoursePageRoutingModule,HeaderPageModule
  ],
  declarations: [ResoursePage]
})
export class ResoursePageModule {}
