import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResoursePage } from './resourse.page';

const routes: Routes = [
  {
    path: '',
    component: ResoursePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResoursePageRoutingModule {}
